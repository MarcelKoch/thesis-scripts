#! /usr/bin/env python3
from main import *
from matplotlib import ticker
from plotting import *
from processing import *
from profilers import *
from functools import partial


float_re = "\d+(\.\d+(e[+-]\d+)?)?|inf"

dof_re = re.compile(r"(gfs_r|gfs) with\s+(?P<dofs>\d+)")
cdof_re = re.compile(r"gfs_rCoarse with\s+(?P<cdofs>\d+)")
it_re = re.compile(r"IT\s+(?P<iterations>\d+)")

runtime_re = re.compile(rf"\|\s+RDTSC Runtime \[s\]\s+\|\s+(?P<runtime>{float_re})")
vec_ratio_re = re.compile(rf"\|\s+Vectorization ratio\s+\|\s+(?P<vec_ratio>{float_re})")
total_fp_re = dict((re.compile(r"\|\s+{}\s+\|[^|]+\|\s+(?P<float_t>\d+)".format(k)), v)
                   for k, v in {"FP_ARITH_INST_RETIRED_SCALAR_DOUBLE": 1,
                                "FP_ARITH_INST_RETIRED_128B_PACKED_DOUBLE": 2,
                                "FP_ARITH_INST_RETIRED_256B_PACKED_DOUBLE": 4,
                                "FP_ARITH_INST_RETIRED_512B_PACKED_DOUBLE": 8}.items())
mem_rd_re = re.compile(r"\|\s+CAS_COUNT_RD\s+\|[^|]+\|\s+(?P<mem_rd>\d+)")
mem_wr_re = re.compile(r"\|\s+CAS_COUNT_WR\s+\|[^|]+\|\s+(?P<mem_wr>\d+)")
instr_re = re.compile(r"\|\s+INSTR_RETIRED_ANY\s+\|[^|]+\|\s+(?P<instructions>\d+)")
cpi_re = re.compile(rf"\|\s+CPI\s+\|\s+(?P<cpi>{float_re})")

kernel_re = re.compile(rf".*KERNEL[\s\w]+(?P<kernel_time>{float_re})")
setup_re = re.compile(rf"SETUP\s+(?P<setup>{float_re})")
residual_assembly_re = re.compile(rf".*residual assembly.*(?P<residual_assembly>{float_re})")
solver_re = re.compile(rf".*rate=(?P<rate>{float_re}).*T=(?P<solve_time>{float_re}).*TIT=(?P<tit>{float_re})"
                       rf".*IT=(?P<solve_it>\d+)")


def _flop_scaling(flops, simd_width):
    return int(flops) * simd_width


regex = [dof_re, cdof_re, it_re, runtime_re, vec_ratio_re, instr_re, cpi_re, kernel_re, setup_re, residual_assembly_re,
         solver_re] + \
         [(k, lambda v: int(v) * 64) for k in [mem_rd_re, mem_wr_re]] + \
         [(k, partial(_flop_scaling, simd_width=v)) for k, v in total_fp_re.items()]

minimal_dofs = 1e3
maximal_dofs = 1e7

peak_bw = 100 * 1e9
peak_flops = 800 * 1e9

table_indices = {'name', 'variant', 'dim', 'ilvl', 'blockSize', 'application', 'gridSize', 'rank', 'dofs', 'cdofs'}


def dofs(size:int, dim: int):
    return (size + 1) ** dim


def is_admissible(size: int, dim: int):
    return minimal_dofs < dofs(size, dim) < maximal_dofs


def result_map(execfile: str, type: str, size: int):
    if "handwritten" in execfile:
        return f"{execfile.replace('apps/', 'results/')}_block-1_{type}_size-{size}"
    else:
        return f"{execfile.replace('apps/', 'results/')}_{type}_size-{size}"


def compute_sizes(dims: List[int], block_sizes: List[int], fixed_size: Optional[int] = None):
    sizes = dict()
    for dim in dims:
        sizes[dim] = dict()
        if dim in dims:
            for block_size in block_sizes:
                if fixed_size:
                    sizes[dim][block_size] = [fixed_size]
                else:
                    possible_sizes = [block_size * 2 ** i for i in range(10)]
                    sizes[dim][block_size] = [s for s in possible_sizes if is_admissible(s, dim)]
            sizes[dim][1] = sizes[dim][min(block_sizes)]
    return sizes


def poisson_run(args, base_name, tool):
    filter = input_filter(args.filter)

    blockSize_re = re.compile(r".*block-(\d+).*")
    dim_re = re.compile(r".*(\d)d.*")

    application = args.type
    sizes = compute_sizes(args.dim, args.block_sizes, args.grid_size)

    all_args = []
    for file in os.scandir(Path(".").resolve()):
        if file.is_file() and base_name in file.name and os.access(file.path, os.X_OK):
            dim = int(dim_re.match(file.name).group(1))
            bs_match = blockSize_re.match(file.name)
            block_size = int(bs_match.group(1)) if bs_match else 1
            try:
                for size in sizes[dim][block_size]:
                    result_file = result_map(file.path, application, size)
                    if filter(result_file.split("/")[-1]):
                        all_args.append(((file.path, result_file, (application, size // block_size)), dofs(size, dim)))
            except KeyError:
                pass
    for new_args, _ in sorted(all_args, key=lambda x: x[1]):
        tool.append_args(*new_args)

    run(tool, dry_run=args.dry_run)


def post_processing(df: pd.DataFrame):
    for col in "blockSize gridSize ilvl".split():
        df.loc[:, col] = df[col].map(lambda x: x.split("-")[1])

    df.loc[:, "dim"] = df.dim.map(lambda x: int(x[0]))

    for col in "dofs cdofs dim iterations rank blockSize gridSize float_t mem_rd mem_wr instructions vec_ratio cpi " \
               "setup residual_assembly rate solve_time tit solve_it ilvl".split():
        if col in df.columns:
            df[col] = pd.to_numeric(df[col])

    df.loc[:, "rank"] = df["rank"].map(lambda x: x % 20)

    mask = ~df.iterations.isna()
    for col in "runtime float_t instructions kernel_time residual_assembly solve_time solve_it tit rate " \
               "mem_rd mem_wr".split():
        df.loc[mask, col] = df.loc[mask, col] / df.iterations[mask]

    df.loc[df.solve_it == 0, "solve_it"] = 1  # count direct solve as one iteration

    return df


def process(files, args: argparse.Namespace):
    df = process_raw_regions(files, "name variant dim ilvl blockSize application gridSize rank".split(), regex)
    df = post_processing(df)
    print(df)
    return df


def local_flop(op: pd.DataFrame):
    op_t = op
    opm_t = to_multiindex(op_t)

    element_degree = 1
    dim = opm_t.index.get_level_values("dim")

    f_dof = opm_t.float_t / opm_t.index.get_level_values("dofs") / ((element_degree + 1) / element_degree) ** dim
    f_dof = f_dof.to_frame().query("blockSize > 1").iloc[:, 0]
    return f_dof.groupby("dim").quantile(0.9)


def local_flop_upper_bound():
    index = pd.Index([2, 3], name="dim")

    def _bound(degree, bw, dim):
        return (2 * (2 * degree + 1) ** dim - 3) * 8 * 800 / bw / (degree + 1) ** dim

    return pd.DataFrame({'TFB': pd.Series([_bound(1, 100, d) for d in index], index=index),
                         'TFB_L3': pd.Series([_bound(1, 375, d) for d in index], index=index)})


def theoretical_dofs(df: pd.DataFrame):
    f_dof = local_flop(df)
    dofs = 1. / ((8 * 3) / peak_bw + (f_dof / peak_flops))
    return dofs


def default_column_args(*, ylabel):
    def _default_column_args(d: int, b: int, s: pd.Series):
        args = dict()
        if b == 1:
            args.update(c='k')
        args.update(ylabel=ylabel.format(d=d, b=b) if ylabel else None)
        args.update(title=f"{d}D")
        return args
    return _default_column_args


def default_legend(b: int, column_args = default_column_args):
    args = column_args(ylabel=None)(0, b, pd.Series(dtype=object))
    args.update(label=rf"$k = {b}$" if b > 1 else "Matrix")
    return args


def visualize_dofs(op: pd.DataFrame, outdir: str):
    opm = to_multiindex(op)
    dofs = opm.index.get_level_values("dofs") / opm.runtime / 1e6
    dofs = apply_quantile(dofs, q=0.1, dropna=False)
    dofs = reshape(dofs, unstack="dim blockSize".split(), keep_indices="dofs")

    for d in dofs.columns.levels[0]:
        def _dof_args(b: int, s: pd.Series):
            def_args = default_column_args(ylabel="MDoF/s")
            return def_args(d, b, s)
        fig, axs = plot_columns(dofs[d],  set_size=dict(fraction=.75),
                                yformatter=ticker.StrMethodFormatter("{x:g}"),
                                yformatter_minor=ticker.StrMethodFormatter("{x:g}"),
                                default_args=dict(logx=True, logy=True, xlabel="Number of DoF", title=None),
                                column_args=_dof_args, legend=default_legend)
        if outdir:
            fig.savefig(Path(outdir) / f"poisson-dofs-{d}.pdf")


def visualize_flops(op: pd.DataFrame, outdir: str):
    opm = to_multiindex(op)

    flops_local = opm.float_t / opm.kernel_time / 1e9
    flops_local = flops_local.to_frame().query('blockSize > 1')
    flops_local = apply_quantile(flops_local, q=0.1, dropna=False).iloc[:, 0]
    flops_local = reshape(flops_local, unstack="dim blockSize".split(), keep_indices="dofs")

    peak_flops = 910 / 20
    fig, axs = plot_columns(flops_local, subplots_args=dict(sharex="none", sharey="all",), legend=default_legend,
                            default_args=dict(logx=True, xlabel="Number of DoF"),
                            column_args=default_column_args(ylabel=r"LINPACK Peak (\%)"),
                            yformatter=ticker.PercentFormatter(xmax=peak_flops, decimals=0, symbol=""))
    if outdir:
        fig.savefig(Path(outdir) / "poisson-flops.pdf")


def flops(op: pd.DataFrame):
    opm = to_multiindex(op)

    flops_local = opm.float_t / opm.kernel_time
    flops_local.name = "flops_local"

    flops_global = opm.float_t / opm.runtime
    flops_global.name = "flops_global"

    return apply_quantile(pd.concat([flops_local, flops_global], axis=1),
                          values="flops_local flops_global".split(), q=0.1)


def compare_runtime(op: pd.DataFrame):
    opm = to_multiindex(op)
    return apply_quantile(pd.concat([opm.runtime, opm.kernel_time], axis=1), q=0.9, dropna=False)


def bandwidth(op: pd.DataFrame):
    time = compare_runtime(op)
    time_transfer = time.runtime - time.kernel_time
    mask = time_transfer < 1e-15
    time_transfer.loc[mask] = time.runtime[mask]
    time_transfer = keep_multiindex(time_transfer, "dim blockSize dofs".split())

    opm = to_multiindex(op)
    mem_volume = opm.mem_rd + opm.mem_wr
    mem_volume = keep_multiindex(mem_volume, "dim blockSize dofs rank".split())
    mem_volume = mem_volume.unstack("rank").max(axis=1)

    bw = mem_volume / time_transfer
    bw.name = "bandwidth"
    return bw


def visualize_bandwidth(op: pd.DataFrame, outdir: str):
    bw = bandwidth(op)
    bw = bw.reset_index()
    bw = bw[bw.dofs >= 1e4]
    bw = to_multiindex(bw).iloc[:, 0]
    bw = bw.unstack("dim blockSize".split())

    fig, axs = plot_columns(bw / 1e9, subplots_args=dict(sharex="all", sharey="all"), legend=default_legend,
                            default_args=dict(logx=True, xlabel="Number of DoF"),
                            column_args=default_column_args(ylabel="GiB/s"))
    if outdir:
        fig.savefig(Path(outdir) / "poisson-bw.pdf")


def visualize_data_volume(op: pd.DataFrame, outdir: str):
    opm = to_multiindex(op)
    mem_volume = opm.mem_rd + opm.mem_wr
    mem_volume = keep_multiindex(mem_volume, "dim blockSize dofs rank".split())
    mem_volume = mem_volume.unstack("rank").max(axis=1)
    mem_volume = mem_volume.to_frame().query("dofs >= 1e4").iloc[:, 0]
    mem_volume = reshape(mem_volume, unstack="dim blockSize".split(), keep_indices="dofs")

    fig, axs = plot_columns(mem_volume / 1e9, subplots_args=dict(sharex="all", sharey="all"), legend=default_legend,
                            yformatter=ticker.StrMethodFormatter("{x:g}"),
                            default_args=dict(logx=True, logy=True, xlabel="Number of DoF"),
                            column_args=default_column_args(ylabel="GiB"))
    for ax in axs.flatten():
        ax.set_xlim(ax.get_xlim())
        ax.plot(ax.get_xlim(), [0.03, 0.03], **plot_format["guideline"])
    if outdir:
        fig.savefig(Path(outdir) / "poisson-data-volume.pdf")


def visualize_solver_time(sv: pd.DataFrame, outdir: str):
    svm = to_multiindex(sv)
    time = apply_quantile(svm.solve_time, q=0.9, dropna=False)
    time = reshape(time, "solve_time", unstack="dim blockSize".split(), keep_indices="dofs")

    def sparse_column_args(*, ylabel):
        def _sparse_column_args(d: int, b: int, s: pd.Series):
            def_args_map = default_column_args(ylabel=ylabel)
            args = def_args_map(d, b, s)
            if b in [1, 8, 64]:
                args.update(alpha=1, ls='-')
            else:
                args.update(alpha=0.75, ls=':')
            return args
        return _sparse_column_args

    fig, axs = plot_columns(time, subplots_args=dict(sharex="none", sharey="all"),
                            legend=lambda b: default_legend(b, sparse_column_args),
                            default_args=dict(logx=True, logy=True, xlabel="Number of DoF"),
                            column_args=sparse_column_args(ylabel="Runtime in s"),
                            yformatter=ticker.StrMethodFormatter("{x:g}"))
    if outdir:
        fig.savefig(Path(outdir) / "poisson-solve-time.pdf")

    fig, axs = plot_columns(time.apply(lambda c: c / time.index, axis=0),
                            legend=lambda b: default_legend(b, sparse_column_args),
                            subplots_args=dict(sharex="none", sharey="all"),
                            default_args=dict(logx=True, logy=True, xlabel="Number of DoF"),
                            column_args=sparse_column_args(ylabel="Runtime per DoF in s"))
    if outdir:
        fig.savefig(Path(outdir) / "poisson-solve-time-dofs.pdf")


def visualize_solver_iterations(sv: pd.DataFrame, outdir: str):
    svm = to_multiindex(sv)
    it = apply_quantile(svm.solve_it, dropna=False)
    it = reshape(it, "solve_it", unstack="dim blockSize".split(), keep_indices="dofs")

    fig, axs = plot_columns(it, subplots_args=dict(sharex="none", sharey="all"), legend=default_legend,
                            default_args=dict(logx=True, logy=True, xlabel="Number of DoF"),
                            column_args=default_column_args(ylabel="Iterations"),
                            yformatter=ticker.StrMethodFormatter("{x:g}"))
    if outdir:
        fig.savefig(Path(outdir) / "poisson-solve-it.pdf")


def visualize_solver_tit(sv: pd.DataFrame, outdir: str):
    svm = to_multiindex(sv)
    tit = apply_quantile(svm.tit, q=0.9, dropna=False)
    tit = reshape(tit, "tit", unstack="dim blockSize".split(), keep_indices="dofs")

    fig, axs = plot_columns(tit.apply(lambda c: tit.index / c / 1e6, axis=0), yformatter=ticker.LogFormatter(),
                            subplots_args=dict(sharex="none", sharey="all"), legend=default_legend,
                            default_args=dict(logx=True, logy=True, xlabel="Number of DoF"),
                            column_args=default_column_args(ylabel="MDoF/s"))
    if outdir:
        fig.savefig(Path(outdir) / "poisson-solve-it-dofs.pdf")


def to_multiindex(df: pd.DataFrame):
    return df.set_index(list(set(df.columns) & table_indices))


def prepare_operator(df: pd.DataFrame):
    op = df[df.application == "operator"].drop(columns="application cdofs".split())
    opm = to_multiindex(op)
    kt = opm.xs(3, level="ilvl").kernel_time
    opm = opm.xs(0, level="ilvl")
    opm.loc[:, "kernel_time"] = kt
    mask = opm.kernel_time.isna()
    opm.loc[mask, "kernel_time"] = opm.loc[mask, "runtime"]
    return opm.reset_index()


def prepare_solver(df: pd.DataFrame):
    solver = df[df.application == "solver"].drop(columns="application cdofs".split())
    mask = solver.tit == float("inf")
    solver.loc[mask, "tit"] = solver.loc[mask, "solve_time"]
    return solver


def visualize(df: pd.DataFrame, args: argparse.Namespace):
    operator = prepare_operator(df)
    solver = prepare_solver(df)

    from multiprocessing import Pool
    with Pool(2) as pool:
        res = [pool.apply_async(visualize_dofs, (operator, args.outdir)),
               pool.apply_async(visualize_flops, (operator, args.outdir)),
               pool.apply_async(visualize_bandwidth, (operator, args.outdir)),
               pool.apply_async(visualize_data_volume, (operator, args.outdir)),
               pool.apply_async(visualize_solver_time, (solver, args.outdir)),
               pool.apply_async(visualize_solver_iterations, (solver, args.outdir)),
               pool.apply_async(visualize_solver_tit,(solver, args.outdir))]
        for r in res:
            r.get()


if __name__ == "__main__":
    base_name = "poisson"

    parser, subparsers = default_parser()
    run_parser = subparsers["run"]

    run_parser.add_argument("-t", "--type", dest="type", type=str, choices=["operator", "solver"], required=True)
    run_parser.add_argument("-b", "--block-sizes", dest="block_sizes", type=int, nargs="+", default=range(8, 9 * 8, 8),
                            choices=range(8, 9 * 8, 8), help="Block grid size")
    run_parser.add_argument("-s", "--grid-size", dest="grid_size", type=int, help="Use fixed fine grid size")
    run_parser.add_argument("-d", "--dim", dest="dim", type=int, nargs="+", default=[2, 3], choices=[2, 3],
                            help="Grid Dimension")

    parsed_args = parser.parse_args()

    os.environ.setdefault("OMP_NUM_THREADS", "1")

    if parsed_args.mode == "run":
        used_filter = parsed_args.filter.copy()
        parsed_args.filter = used_filter + ["ilvl-0"]
        tool = PerfCounter(parsed_args.np, None, None, group="MEM_DP", instructed=True)
        poisson_run(parsed_args, base_name, tool)
        if parsed_args.type == "operator":
            parsed_args.filter = used_filter + ["ilvl-3"]
            tool = Pin(parsed_args.np, None, None)
            poisson_run(parsed_args, base_name, tool)
    elif parsed_args.mode == "process":
        default_process(parsed_args, base_name, process)
    elif parsed_args.mode == "visualize":
        default_visualize(parsed_args, visualize)
    else:
        parser.print_help()
