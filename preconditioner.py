#! /usr/bin/env python3
from main import *
from matplotlib.ticker import LogFormatter
from plotting import *
from processing import *
from profilers import *


dof_def_re = re.compile(r"DOF\s+(?P<dofs>\d+)")
cdof_def_re = re.compile(r"CDOF\s+(?P<cdofs>\d+)")
coarse_n_re = re.compile(r"N\s+(?P<coarse_n>\d+)")
fine_n_re = re.compile(r"n\s+(?P<fine_n>\d+)")
iteration_re = re.compile(r".*rate=(?P<rate>\d+(\.\d+(e[+-]\d+)?)?).*T=(?P<t>\d+(\.\d+(e[+-]\d+)?)?)"
                          r".*TIT=(?P<tit>\d+(\.\d+(e[+-]\d+)?)?|inf).*IT=(?P<it>\d+)")

regex = [dof_def_re, cdof_def_re, coarse_n_re, fine_n_re, iteration_re]


def block_size(file):
    return int(file.split("-")[-1])


def result_map(execfile: str, m: int):
    return f"{execfile.replace('src/', 'results/')}_grid-{m}"


def call_args(name: str, m: int):
    return m,


def post_processing(df: pd.DataFrame):
    for col in "block grid".split():
        df.loc[:, col] = df[col].map(lambda x: x.split("-")[1])

    for col in "dofs cdofs coarse_n fine_n t tit it block grid".split():
        df[col] = pd.to_numeric(df[col])

    return df


def process(files, args: argparse.Namespace):
    df = process_raw(files, "type block grid".split(), regex)
    df = post_processing(df)
    return df


def const_mesh(df: pd.DataFrame):
    df = df[df.fine_n == 1024]
    df = df.drop(columns="grid coarse_n fine_n dofs cdofs rate t tit".split())
    df = df.pivot(index="block", columns="type", values="it")
    return df


def const_block(df: pd.DataFrame):
    df = df[df.block == 16]
    df = df.drop(columns="block coarse_n grid dofs cdofs rate t tit".split())
    df = df.pivot(index="fine_n", columns="type", values="it")
    return df


def visualize(df: pd.DataFrame, args: argparse.Namespace):
    cm = const_mesh(df)
    cb = const_block(df)

    combined = pd.concat([cm, cb], axis=1, keys="mesh block".split())

    def column_args(v: str, t: str, s: pd.Series):
        title = {"mesh": r"Fixed $h$", "block": r"Fixed $H/h$"}.get(v, None)
        label = {"jacobi": "Jacobi", "nn": "Neumann-Neumann"}.get(t, None)
        if v == "mesh":
            return dict(logy=True, title=title, xlabel=r"$H/h$", label=label)
        else:
            return dict(title=title, xlabel="Number of Fine Grid Elements", ylabel="Iterations", label=label)

    fig, axs = plot_columns(combined, subplots_args=dict(sharex="none", sharey="none"),
                            default_args=dict(logx=True),
                            column_args=column_args)
    axs[0, 1].get_yaxis().set_major_formatter(LogFormatter())
    axs[0, 1].get_yaxis().set_minor_formatter(LogFormatter())
    axs[0, 0].legend()
    fig.savefig(Path(args.outdir) / "preconditioner.pdf")


if __name__ == "__main__":
    parser, sub_parser = default_parser()
    parser_run = sub_parser["run"]

    group = parser_run.add_mutually_exclusive_group(required=True)
    group.add_argument("-m", "--mesh_size", action="store_true", help="Vary mesh size by fixed block size = 16")
    group.add_argument("-b", "--block_size", action="store_true", help="Vary block size by fixed mesh size = 1024")

    args = parser.parse_args()

    os.environ["OMP_NUM_THREADS"] = "1"

    fixed_mesh_size = 1024
    fixed_block_size = 16

    if args.mode == "run":
        tool = NOPIN()
        filter = input_filter(args.filter)

        viable_file = []
        for file in os.scandir(Path(".").resolve()):
            if file.is_file() and os.access(file.path, os.X_OK) and filter(file.name):
                viable_file.append(file)

        if args.block_size or not args.mesh_size:
            for file in viable_file:
                blocks = block_size(file.name)
                mesh_size = fixed_mesh_size // blocks
                tool.append_args(file.path, result_map(file.path, mesh_size), call_args(file.path, mesh_size))
        if args.mesh_size or not args.block_size:
            filenames = [file for file in viable_file if file.name.endswith("16")]
            for mesh_level in range(2, 8):
                mesh_size = 2**mesh_level
                for file in filenames:
                    tool.append_args(file.path, result_map(file.path, mesh_size), call_args(file.path, mesh_size))

        run(tool, dry_run=args.dry_run)
    elif args.mode == "process":
        default_process(args, "", process)
    elif args.mode == "visualize":
        default_visualize(args, visualize)
    else:
        parser.print_help()
