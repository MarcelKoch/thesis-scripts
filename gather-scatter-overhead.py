#! /usr/bin/env python3
from main import *
from plotting import *
from profilers import *
from processing import *
import re


dof_def_re = re.compile(r"DOF\s+(?P<dofs>\d+)")
it_def_re = re.compile(r"IT\s+(?P<iterations>\d+)")
runtime_def_re = re.compile(r"TIME\s+(?P<runtime>\d+(\.\d+(e[+-]\d+)?)?)")

regex = [dof_def_re, it_def_re, runtime_def_re]


def result_map(execfile: str, p: int):
    return f"{execfile.replace('src/', 'results/')}_{p}"


def call_args(_: str):
    return tuple()


def post_processing(df: pd.DataFrame):
    for col in "blockSize quadOrder".split():
        df.loc[:, col] = df[col].map(lambda x: x.split("-")[1])

    df.loc[:, "runtime"] = df.runtime / df.iterations

    df.loc[:, "rank"] = df["rank"].map(lambda x: x % 20)

    for col in "dofs iterations rank blockSize quadOrder".split():
        df[col] = pd.to_numeric(df[col])

    return df


def process(files, args: argparse.Namespace):
    df = process_raw(files, "model mode grid blockSize quadOrder rank".split(), regex)
    df = post_processing(df)
    return df


def local_kernel_percentage(df: pd.DataFrame):
    df = df.drop(columns="grid model iterations dofs".split())
    p = pivot(df, "quadOrder mode".split())
    dfp = pd.DataFrame()
    for q in p.columns.levels[1]:
        dfp.loc[:, q] = p["runtime"][q]["local"] / p["runtime"][q]["global"]
        dfp.loc[:, q] = dfp[q].map(lambda x: x if x < 1 else 1)
    dfp = apply_quantile(dfp, q=0.75, values=list(p.columns.levels[1]))
    return dfp


def global_runtime(df: pd.DataFrame):
    df = df[df["mode"] == "global"]
    df = df.drop(columns="grid model mode iterations dofs".split())
    df = apply_quantile(df, q=0.75)
    df = pivot(df, "quadOrder".split())

    df = df["runtime"]

    df_normalized = df.copy()
    for c in df_normalized.columns:
        df_normalized[c] = df_normalized[c] / (df_normalized[c][1])
    return df, df_normalized


def visualize(df: pd.DataFrame, args: argparse.Namespace):
    dfp = local_kernel_percentage(df)
    print(dfp)
    dfs, dfs_normalized = global_runtime(df)
    dfs = dfs.rename_axis(columns={"quadOrder": "Quadrature Order"}, index={"blockSize": "Block Size"})
    print(dfs)
    dfs.to_latex(base_path / "tbl/gather-scatter-runtime.tex")

    def column_args(qo: int, s: pd.Series):
        return dict(label=f"$q={qo}$", marker=".")

    fig, axs = plot_columns(dfp * 100, set_size=dict(fraction=0.75), column_args=column_args,
                           default_args=dict(logx=True, ylabel="Local Kernel Runtime (\%)", xlabel="Block Size"))
    axs[0, 0].legend(loc="lower right", ncol=3)
    fig.savefig(base_path / "img/gs-percentage.pdf")

    fig, axs = plot_columns(dfs_normalized * 100, set_size=dict(fraction=0.75), column_args=column_args,
                           default_args=dict(logx=True, ylabel="Total Runtime (\%)", xlabel="Block Size"))
    axs[0, 0].legend(loc="lower right", ncol=3)
    fig.savefig(base_path / "img/gs-runtime.pdf")


if __name__ == "__main__":
    base_name = "gs-overhead"

    parser, _ = default_parser()
    parsed_args = parser.parse_args()

    tool = Pin(parsed_args.np, call_args, result_map)

    main(base_name=base_name, tool=tool, process=process, visualize=visualize, parser=parser, parsed_args=parsed_args)
