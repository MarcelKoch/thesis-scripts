#! /usr/bin/env python3
from geometry_transformation import *


def process(files, args: argparse.Namespace):
    df = process_raw(files, "model mode dim grid blockSize rank".split(), regex)
    df = post_processing(df)
    df.loc[:, "dim"] = pd.to_numeric(df.dim.apply(lambda s: s.split("-")[1]))
    return df


def plot_runtime(rt: pd.DataFrame):
    rt = rt.reorder_levels(["dim", "grid", "mode"], axis=1)

    def column_args(dim: int, geo: str, mode: str, s: pd.Series):
        args = dict()
        if dim == 2:
            label = mode.capitalize() if mode else None
            args.update(title=f"{geo.capitalize()} Geometry" if geo else None, label=label)
        if geo == "affine":
            args.update(ylabel=f"{dim}D\nKernel Runtime (\%)")
        if mode == "generated":
            args.update(marker="v", ls="-.")
        else:
            args.update(marker="^", ls=":")
        return args

    fig, axs = plot_columns(rt * 100, set_size=True, legend=False, subplots_args=dict(sharey="all", sharex="none"),
                            default_args=dict(logx=True, xlabel="Block Size"),
                            column_args=column_args, yformatter=PercentFormatter(symbol=""))
    for ax in axs.flatten():
        ax.axhline(100, **plot_format["guideline"])
    axs[0, 1].legend()
    fig.savefig(base_path / "img/gen-geo.pdf", format="pdf", transparent=True, bbox_inches="tight")


def visualize(df: pd.DataFrame, args: argparse.Namespace):
    rt = runtime_fraction(df)
    plot_runtime(rt)


if __name__ == "__main__":
    base_name = "licm"

    parser, _ = default_parser()
    parsed_args = parser.parse_args()

    tool = PerfCounter(parsed_args.np, call_args, result_map, group="FLOPS_DP", instructed=False)

    main(base_name=base_name, tool=tool, process=process, visualize=visualize, parser=parser, parsed_args=parsed_args)
