#! /usr/bin/env python3
from functools import partial
from main import *
from matplotlib.ticker import PercentFormatter, StrMethodFormatter
from plotting import *
from profilers import *
from processing import *


dof_re = re.compile(r"DOF\s+(?P<dofs>\d+)")
it_re = re.compile(r"IT\s+(?P<iterations>\d+)")
runtime_re = re.compile(r"TIME\s+(?P<runtime>\d+(\.\d+(e[+-]\d+)?)?)")
local_runtime_re = re.compile(r"(\W|\d)*LOCAL_TIME[^\d]+(?P<local_runtime>\d+(\.\d+(e[+-]\d+)?)?)")

vec_ratio_re = re.compile(r"\|\s+Vectorization ratio\s+\|\s+(?P<vec_ratio>\d+(\.\d+(e[+-]\d+)?)?)")
total_fp_re = dict((re.compile(r"\|\s+{}\s+\|[^|]+\|\s+(?P<float_t>\d+)".format(k)), v)
                   for k, v in {"FP_ARITH_INST_RETIRED_SCALAR_DOUBLE": 1,
                                "FP_ARITH_INST_RETIRED_128B_PACKED_DOUBLE": 2,
                                "FP_ARITH_INST_RETIRED_256B_PACKED_DOUBLE": 4,
                                "FP_ARITH_INST_RETIRED_512B_PACKED_DOUBLE": 8}.items())


def _flop_scaling(flops, simd_width):
    return int(flops) * simd_width


regex = [dof_re, it_re, runtime_re, local_runtime_re, vec_ratio_re] + \
         [(k, partial(_flop_scaling, simd_width=v)) for k, v in total_fp_re.items()]

table_indices = set("model variant loopOrder dim grid blockSize quadOrder rank dofs".split())

matrix_dofs = {"2D": 100 / (2 * (3 ** 2) * 8) * 1e3 / 20,
               "3D": 100 / (2 * (3 ** 3) * 8) * 1e3 / 20}


def result_map(execfile: str, p: int):
    return f"{execfile.replace('src/', 'results/')}_{p}"


def call_args(_: str):
    return tuple()


def post_processing(df: pd.DataFrame):
    for col in "blockSize quadOrder".split():
        df.loc[:, col] = df[col].map(lambda x: x.split("-")[1])

    for col in "dofs rank blockSize quadOrder runtime float_t".split():
        df.loc[:, col] = pd.to_numeric(df[col])

    for col in "runtime local_runtime float_t".split():
        df.loc[:, col] = df[col] / df.iterations

    df = df.drop(columns="iterations")

    df.loc[:, "rank"] = df["rank"].map(lambda x: x % 20)

    return df


def process(files, args: argparse.Namespace):
    df = process_raw(files, "model loopOrder dim grid blockSize quadOrder rank".split(), regex)
    df = post_processing(df)
    return df


def column_args(lo: str, qo: int, s: pd.Series):
    args = dict(title=lo.capitalize() if lo else None)
    return args


def legend(qo: int):
    return dict(label=rf"$q = {qo}$")


def visualize_flops(dfm: pd.DataFrame, outdir: str = ""):
    flops = dfm.float_t / dfm.local_runtime
    flops = apply_quantile(flops, q=0.1) / 1e9
    flops = reshape(flops, unstack="loopOrder quadOrder".split(), keep_indices="blockSize")

    peak_flops = 910 / 20
    fig, axs = plot_columns(flops, subplots_args=dict(sharex="all", sharey="all"), legend=legend, set_size=True,
                            default_args=dict(logx=True, xlabel="Block Size", ylabel=r"LINPACK Peak (\%)"),
                            column_args=column_args, yformatter=PercentFormatter(xmax=peak_flops, decimals=0,
                                                                                 symbol=""))
    for ax in axs.flatten():
        ax.axhline(peak_flops, **plot_format["guideline"])
    if outdir:
        fig.savefig(Path(outdir) / "loop-order-flops.pdf")


def visualize_dofs(dfm: pd.DataFrame, outdir: str = ""):
    dofs = dfm.index.get_level_values("dofs") / dfm.runtime
    dofs = apply_quantile(dofs, q=0.9) / 1e6
    dofs = reshape(dofs, unstack="loopOrder quadOrder".split(), keep_indices="blockSize")

    fig, axs = plot_columns(dofs, subplots_args=dict(sharex="all", sharey="all"), legend=legend, set_size=True,
                            default_args=dict(logx=True, logy=True, xlabel="Block Size", ylabel="MDoF/s"),
                            column_args=column_args, yformatter=StrMethodFormatter("{x:g}"))
    for ax in axs.flatten():
        ax.axhline(matrix_dofs["2D"], **plot_format["guideline"])
    if outdir:
        fig.savefig(Path(outdir) / "loop-order-dofs.pdf")


def to_multiindex(df: pd.DataFrame):
    return df.set_index(list(set(df.columns) & table_indices))


def prepare(df: pd.DataFrame):
    dfm = to_multiindex(df)
    dfm = keep_multiindex(dfm, "dim variant blockSize quadOrder loopOrder dofs rank".split())
    return dfm


def visualize(df: pd.DataFrame, args: argparse.Namespace):
    dfm = prepare(df)
    visualize_flops(dfm, args.outdir)
    visualize_dofs(dfm, args.outdir)


if __name__ == "__main__":
    base_name = "loop-order"

    parser, _ = default_parser()
    parsed_args = parser.parse_args()

    if parsed_args.mode == "run":
        tool = PerfCounter(parsed_args.np, call_args, result_map, group="FLOPS_DP", instructed=False)
    else:
        tool = None

    main(base_name=base_name, tool=tool, process=process, visualize=visualize, parser=parser, parsed_args=parsed_args)
