#! /usr/bin/env python3
from main import *
from matplotlib import ticker
from plotting import *
from processing import *
from profilers import *
from functools import partial


float_re = "\d+(\.\d+(e[+-]\d+)?)?|inf"

dof_re = re.compile(r"(gfs_r|gfs) with\s+(?P<dofs>\d+)")
cdof_re = re.compile(r"gfs_rCoarse with\s+(?P<cdofs>\d+)")
it_re = re.compile(r"IT\s+(?P<iterations>\d+)")

runtime_re = re.compile(rf"\|\s+RDTSC Runtime \[s\]\s+\|\s+(?P<runtime>{float_re})")
vec_ratio_re = re.compile(rf"\|\s+Vectorization ratio\s+\|\s+(?P<vec_ratio>{float_re})")
total_fp_re = dict((re.compile(r"\|\s+{}\s+\|[^|]+\|\s+(?P<float_t>\d+)".format(k)), v)
                   for k, v in {"FP_ARITH_INST_RETIRED_SCALAR_DOUBLE": 1,
                                "FP_ARITH_INST_RETIRED_128B_PACKED_DOUBLE": 2,
                                "FP_ARITH_INST_RETIRED_256B_PACKED_DOUBLE": 4,
                                "FP_ARITH_INST_RETIRED_512B_PACKED_DOUBLE": 8}.items())
mem_rd_re = re.compile(r"\|\s+CAS_COUNT_RD\s+\|[^|]+\|\s+(?P<mem_rd>\d+)")
mem_wr_re = re.compile(r"\|\s+CAS_COUNT_WR\s+\|[^|]+\|\s+(?P<mem_wr>\d+)")
instr_re = re.compile(r"\|\s+INSTR_RETIRED_ANY\s+\|[^|]+\|\s+(?P<instructions>\d+)")
cpi_re = re.compile(rf"\|\s+CPI\s+\|\s+(?P<cpi>{float_re})")

kernel_re = re.compile(rf".*KERNEL[\s\w]+(?P<kernel_time>{float_re})")
solver_re = re.compile(rf".*rate=(?P<rate>{float_re}).*T=(?P<solve_time>{float_re}).*TIT=(?P<tit>{float_re})"
                       rf".*IT=(?P<solve_it>\d+)")
newton_re = re.compile((r"\s*Newton\s*converged\s*after\s*(?P<newton_it>\d+)"))


def _flop_scaling(flops, simd_width):
    return int(flops) * simd_width


regex = [dof_re, cdof_re, it_re, runtime_re, vec_ratio_re, instr_re, cpi_re, kernel_re, solver_re, newton_re]\
        + [(k, lambda v: int(v) * 64) for k in [mem_rd_re, mem_wr_re]]\
        + [(k, partial(_flop_scaling, simd_width=v)) for k, v in total_fp_re.items()]

minimal_dofs = 1e3
maximal_dofs = 1e7

peak_bw = 100 * 1e9
peak_flops = 800 * 1e9

table_indices = {'name', 'variant', 'ilvl', 'blockSize', 'application', 'region', 'gridSize', 'rank', 'dofs', 'cdofs'}


def dofs(size:int, dim: int):
    return (size + 1) ** dim


def is_admissible(size: int, dim: int):
    return minimal_dofs < dofs(size, dim) < maximal_dofs


def result_map(execfile: str, type: str, size: int):
    if "handwritten" in execfile:
        return f"{execfile.replace('apps/', 'results/')}_block-1_{type}_size-{size}"
    else:
        return f"{execfile.replace('apps/', 'results/')}_{type}_size-{size}"


def compute_sizes(block_sizes: List[int], fixed_size: Optional[int] = None):
    sizes = dict()
    for block_size in block_sizes:
        if fixed_size:
            sizes[block_size] = [fixed_size]
        else:
            possible_sizes = [block_size * 2 ** i for i in range(10)]
            sizes[block_size] = [s for s in possible_sizes if is_admissible(s, 2)]
    sizes[1] = sizes[min(block_sizes)]
    return sizes


def plaplacian_run(args, base_name, tool):
    filter = input_filter(args.filter)

    blockSize_re = re.compile(r".*block-(\d+).*")

    application = args.type
    sizes = compute_sizes(args.block_sizes, args.grid_size)

    all_args = []
    for file in os.scandir(Path(".").resolve()):
        if file.is_file() and base_name in file.name and os.access(file.path, os.X_OK):
            bs_match = blockSize_re.match(file.name)
            block_size = int(bs_match.group(1)) if bs_match else 1
            try:
                for size in sizes[block_size]:
                    result_file = result_map(file.path, application, size)
                    if filter(result_file.split("/")[-1]):
                        all_args.append(((file.path, result_file, (application, size // block_size)), dofs(size, 2)))
            except KeyError:
                pass
    for new_args, _ in sorted(all_args, key=lambda x: x[1]):
        tool.append_args(*new_args)

    run(tool, dry_run=args.dry_run)


def post_processing(df: pd.DataFrame):
    for col in "blockSize gridSize ilvl".split():
        df.loc[:, col] = df[col].map(lambda x: x.split("-")[1])

    for col in "dofs cdofs iterations rank blockSize gridSize float_t mem_rd mem_wr instructions vec_ratio cpi " \
               "rate solve_time tit solve_it ilvl".split():
        if col in df.columns:
            df[col] = pd.to_numeric(df[col])

    df.loc[:, "rank"] = df["rank"].map(lambda x: x % 20)

    mask = ~df.iterations.isna()
    for col in "runtime float_t instructions kernel_time solve_time solve_it tit rate " \
               "mem_rd mem_wr newton_it".split():
        df.loc[mask, col] = df.loc[mask, col] / df.iterations[mask]

    df.loc[df.solve_it == 0, "solve_it"] = 1  # count direct solve as one iteration

    df.loc[df.application == "solver", "region"] = "solver"

    return df


def process(files, args: argparse.Namespace):
    df = process_raw_regions(files, "name variant ilvl blockSize application gridSize rank".split(), regex,
                             ["operator", "assemble"])
    df = post_processing(df)
    print(df)
    return df


def local_flop(op: pd.DataFrame):
    op_t = op
    opm_t = to_multiindex(op_t)

    element_degree = 1
    dim = 2

    f_dof = opm_t.float_t / opm_t.index.get_level_values("dofs") / ((element_degree + 1) / element_degree) ** dim
    f_dof = f_dof.to_frame().query("blockSize > 1").iloc[:, 0]
    return f_dof.quantile(0.9)


def local_flop_upper_bound():
    index = pd.Index([2, 3], name="dim")

    def _bound(degree, bw, dim, alpha):
        return ((2 + alpha) * (2 * degree + 1) ** dim - 3 - 1) * 8 * 800 / bw / (degree + 1) ** dim

    return pd.DataFrame({'Best Case': pd.Series([_bound(1, 100, d, 0) for d in index], index=index),
                         'Worst Case': pd.Series([_bound(1, 100, d, 1) for d in index], index=index)})


def default_column_args(*, ylabel):
    def _default_column_args(b: int, s: pd.Series):
        styles = {1: dict(marker=".", color="k"),
                  8: dict(marker="1"),
                  16: dict(marker="2"),
                  32: dict(marker="3"),
                  64: dict(marker="4")}
        args = styles[b]
        args.update(ylabel=ylabel.format(b=b) if ylabel else None)
        return args
    return _default_column_args


def default_legend(b: int):
    args = default_column_args(ylabel=None)(b, pd.Series(dtype=object))
    args.update(label=rf"$k = {b}$" if b > 1 else "Matrix")
    return args


def visualize_dofs(op: pd.DataFrame, outdir: str):
    opm = to_multiindex(op)
    dofs = opm.index.get_level_values("dofs") / opm.runtime / 1e6
    dofs = apply_quantile(dofs, q=0.1, dropna=False)
    dofs = reshape(dofs, unstack="blockSize", keep_indices="dofs")

    fig, axs = plot_columns(dofs, yformatter=ticker.LogFormatter(), set_size=dict(fraction=0.75), legend=default_legend,
                            default_args=dict(logx=True, logy=True, xlabel="Number of DoF", title=None),
                            column_args=default_column_args(ylabel="MDoF/s"))
    if outdir:
        fig.savefig(Path(outdir) / f"p-laplacian-dofs.pdf")

def visualize_free_iterations(op: pd.DataFrame, asm: pd.DataFrame, outdir: str = ""):
    asm_rt = apply_quantile(to_multiindex(asm).runtime)
    asm_rt = keep_multiindex(asm_rt, "dofs")

    opm = to_multiindex(op)
    op_rt = apply_quantile(opm.runtime)

    mb_rt = keep_multiindex(op_rt.xs(1, level="blockSize"), "dofs")
    mf_rf = reshape(op_rt.to_frame().query("blockSize > 1").iloc[:, 0], unstack="blockSize", keep_indices="dofs")

    free_iterations = mf_rf.apply(lambda c: asm_rt / (c - mb_rt), axis=0)

    # consider full solver iteration inclusive preconditioner application
    #worst_case = mf_rf.apply(lambda c: asm_rt / (c * 4 - mb_rt * 2), axis=0)  # matrix preconditioner is cheap
    #best_case = mf_rf.apply(lambda c: asm_rt / (c * 3), axis=0)  # matrix-based as fast as matrix-free

    fig, axs = plot_columns(free_iterations, set_size=dict(fraction=0.75), legend=default_legend,
                            default_args=dict(logx=True, xlabel="Number of DoF", title=None),
                            column_args=default_column_args(ylabel="Iterations"))
    if outdir:
        fig.savefig(Path(outdir) / f"p-laplacian-free-iterations.pdf")


def visualize_solver_iterations(sv: pd.DataFrame, asm: pd.DataFrame, outdir: str = ""):
    svm = to_multiindex(sv)
    svm_q = apply_quantile(svm.loc[:, ["solve_it", "newton_it", "solve_time"]], dropna=False)
    it = svm_q.solve_it / svm_q.newton_it
    it = reshape(it, unstack="blockSize", keep_indices="dofs")

    rt_ls = svm_q.solve_time / svm_q.newton_it
    rt_ls = reshape(rt_ls, unstack="blockSize", keep_indices="dofs")

    asm_rt = apply_quantile(to_multiindex(asm).runtime)
    asm_rt = keep_multiindex(asm_rt, "dofs")

    rt_ls.loc[:, 1] = rt_ls[1] + asm_rt

    tit = rt_ls / it

    combined = pd.concat([it, tit.apply(lambda c: tit.index / c / 1e6, axis=0)], axis=1, keys=["it", "tit"])

    def column_args(t: str, b: int, s: pd.Series):
        ylabel = {"it": "Iterations", "tit":"MDoF/s"}.get(t, None)
        args = default_column_args(ylabel=ylabel)(b, s)
        return args

    from matplotlib.ticker import StrMethodFormatter
    fig, axs = plot_columns(combined, subplots_args=dict(sharex="all", sharey="none"),
                            default_args=dict(logx=True, logy=True, xlabel="Number of DoF"), column_args=column_args,
                            set_size=False, legend=False)
    axs[0, 1].yaxis.set_label_position("right")
    axs[0, 1].yaxis.tick_right()
    # set formatter explicitly to be recognized by set_size
    axs[0, 0].yaxis.set_major_formatter(ticker.LogFormatter())
    axs[0, 1].yaxis.set_major_formatter(StrMethodFormatter("{x:g}"))
    axs[0, 1].yaxis.set_minor_formatter(StrMethodFormatter("{x:g}"))
    fig, axs = set_legend(fig, axs, combined, default_legend, set_size=True)
    if outdir:
        fig.savefig(Path(outdir) / "p-laplacian-solve-it.pdf")


def visualize_solver_runtime(sv: pd.DataFrame, asm: pd.DataFrame, outdir: str = ""):
    svm = to_multiindex(sv)
    svm_q = apply_quantile(svm.loc[:, ["newton_it", "solve_time", "runtime"]], dropna=False)

    rt_ls = svm_q.solve_time / svm_q.newton_it
    rt_ls = reshape(rt_ls, unstack="blockSize", keep_indices="dofs")

    asm_rt = apply_quantile(to_multiindex(asm).runtime)
    asm_rt = keep_multiindex(asm_rt, "dofs")

    rt_ls.loc[:, 1] = rt_ls[1] + asm_rt

    rt_newton = svm_q.runtime / svm_q.newton_it
    rt_newton = reshape(rt_newton, unstack="blockSize", keep_indices="dofs")

    combined = pd.concat([rt_ls.apply(lambda c: c / rt_ls.index, axis=0),
                          rt_newton.apply(lambda c: c / rt_newton.index, axis=0)],
                         axis=1, keys=["rt_ls", "rt_newton"])

    def column_args(t: str, b: int, s: pd.Series):
        ylabel = "Runtime per DoF in s" if t == "rt_ls" else None
        title = {"rt_ls": "Linear Solver", "rt_newton": "Newton Step"}.get(t, None)
        args = default_column_args(ylabel=ylabel)(b, s)
        args.update(title=title)
        return args

    fig, axs = plot_columns(combined, legend=default_legend, subplots_args=dict(sharex="all", sharey="none"),
                            default_args=dict(logx=True, logy=True, xlabel="Number of DoF"), column_args=column_args)
    if outdir:
        fig.savefig(Path(outdir) / "p-laplacian-solve-time-dofs.pdf")


def to_multiindex(df: pd.DataFrame):
    return df.set_index(list(set(df.columns) & table_indices))


def prepare_operator(df: pd.DataFrame):
    op = df[df.region == "operator"].drop(columns="application region cdofs".split())
    opm = to_multiindex(op)
    kt = opm.xs(3, level="ilvl").kernel_time
    opm = opm.xs(0, level="ilvl")
    opm.loc[:, "kernel_time"] = kt
    mask = opm.kernel_time.isna()
    opm.loc[mask, "kernel_time"] = opm.loc[mask, "runtime"]
    return opm.reset_index()


def prepare_assemble(df: pd.DataFrame):
    asm = df[df.region == "assemble"].drop(columns="application region cdofs ".split())
    return asm


def prepare_solver(df: pd.DataFrame):
    solver = df[df.region == "solver"].drop(columns="application region cdofs ".split())
    mask = solver.solve_it < solver.newton_it
    solver.loc[mask, "solve_it"] = solver.loc[mask, "newton_it"]
    return solver


def visualize(df: pd.DataFrame, args: argparse.Namespace):
    operator = prepare_operator(df)
    asm = prepare_assemble(df)
    solver = prepare_solver(df)

    print(f"Local FLOP measured:\n\t{local_flop(operator)}")
    print(f"Local FLOP bound:\n{local_flop_upper_bound()}")

    from multiprocessing import Pool
    with Pool() as pool:
        res = [pool.apply_async(visualize_dofs, (operator, args.outdir)),
               pool.apply_async(visualize_free_iterations, (operator, asm, args.outdir)),
               pool.apply_async(visualize_solver_runtime, (solver, asm, args.outdir)),
               pool.apply_async(visualize_solver_iterations, (solver, asm, args.outdir))]
        for r in res:
            r.get()


if __name__ == "__main__":
    base_name = "p-laplacian"

    parser, subparsers = default_parser()
    run_parser = subparsers["run"]

    run_parser.add_argument("-t", "--type", dest="type", type=str, choices=["operator", "solver"], required=True)
    run_parser.add_argument("-b", "--block-sizes", dest="block_sizes", type=int, nargs="+", default=[8, 16, 32, 64],
                            choices=[8, 16, 32, 64], help="Block grid size")
    run_parser.add_argument("-s", "--grid-size", dest="grid_size", type=int, help="Use fixed fine grid size")

    parsed_args = parser.parse_args()

    os.environ.setdefault("OMP_NUM_THREADS", "1")

    if parsed_args.mode == "run":
        used_filter = parsed_args.filter.copy()
        parsed_args.filter = used_filter + ["ilvl-0"]
        tool = PerfCounter(parsed_args.np, None, None, group="MEM_DP", instructed=True)
        plaplacian_run(parsed_args, base_name, tool)
        if parsed_args.type == "operator":
            parsed_args.filter = used_filter + ["ilvl-3"]
            tool = Pin(parsed_args.np, None, None)
            plaplacian_run(parsed_args, base_name, tool)
    elif parsed_args.mode == "process":
        default_process(parsed_args, base_name, process)
    elif parsed_args.mode == "visualize":
        default_visualize(parsed_args, visualize)
    else:
        parser.print_help()
