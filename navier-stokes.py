#! /usr/bin/env python3
from functools import partial

from matplotlib import ticker

from main import *
from plotting import *
from processing import *
from profilers import *

matplotlib.use("pgf")  # necessary since execute_constrained_layout does not recognize PGF preamble otherwise

float_re = "\d+(\.\d+(e[+-]\d+)?)?|inf"

dof_u_re = re.compile(r"(gfs_u|gfs_F1) with\s+(?P<dofs_u>\d+)")
dof_p_re = re.compile(r"(gfs_p|gfs_F2) with\s+(?P<dofs_p>\d+)")
cdof_u_re = re.compile(r"gfs_F1Coarse with\s+(?P<cdofs_u>\d+)")
cdof_p_re = re.compile(r"gfs_F2Coarse with\s+(?P<cdofs_p>\d+)")
it_re = re.compile(r"IT\s+(?P<iterations>\d+)")

runtime_re = re.compile(rf"\|\s+RDTSC Runtime \[s\]\s+\|\s+(?P<runtime>{float_re})")
vec_ratio_re = re.compile(rf"\|\s+Vectorization ratio\s+\|\s+(?P<vec_ratio>{float_re})")
total_fp_re = dict((re.compile(r"\|\s+{}\s+\|[^|]+\|\s+(?P<float_t>\d+)".format(k)), v)
                   for k, v in {"FP_ARITH_INST_RETIRED_SCALAR_DOUBLE": 1,
                                "FP_ARITH_INST_RETIRED_128B_PACKED_DOUBLE": 2,
                                "FP_ARITH_INST_RETIRED_256B_PACKED_DOUBLE": 4,
                                "FP_ARITH_INST_RETIRED_512B_PACKED_DOUBLE": 8}.items())
mem_rd_re = re.compile(r"\|\s+CAS_COUNT_RD\s+\|[^|]+\|\s+(?P<mem_rd>\d+)")
mem_wr_re = re.compile(r"\|\s+CAS_COUNT_WR\s+\|[^|]+\|\s+(?P<mem_wr>\d+)")
instr_re = re.compile(r"\|\s+INSTR_RETIRED_ANY\s+\|[^|]+\|\s+(?P<instructions>\d+)")
cpi_re = re.compile(rf"\|\s+CPI\s+\|\s+(?P<cpi>{float_re})")

kernel_re = re.compile(rf".*KERNEL[\s\w]+(?P<kernel_time>{float_re})")
solver_re = re.compile(r"SOLVER_IT\s+(?P<solver_iterations>\d+)")
time_loop_re = re.compile(r"TIME_LOOP\s+(?P<time_loop>\d+)")


def _flop_scaling(flops, simd_width):
    return int(flops) * simd_width


regex = [dof_u_re, dof_p_re, cdof_u_re, cdof_p_re, it_re, runtime_re, vec_ratio_re, mem_rd_re, mem_wr_re, instr_re,
         cpi_re, kernel_re, solver_re, time_loop_re] + \
        [(k, lambda v: int(v) * 64) for k in [mem_rd_re, mem_wr_re]] + \
        [(k, partial(_flop_scaling, simd_width=v)) for k, v in total_fp_re.items()]

table_indices = {'name', 'variant', 'region', 'element', 'ilvl', 'blockSize', 'application', 'gridLevel', 'rank',
                 'dofs', 'cdofs_u', 'cdofs_p', }


def int_log2(i: int):
    return i.bit_length() - 1


def result_map(execfile: str, application: str, level: int):
    if "handwritten" in execfile:
        return f"{execfile.replace('apps/', 'results/')}_block-1_{application}_level-{level}"
    else:
        return f"{execfile.replace('apps/', 'results/')}_{application}_level-{level}"


def compute_levels(elements: List[str], block_sizes: List[int], grid_level: Optional[int]):
    levels = dict()
    all_block_sizes = [1] + block_sizes
    max_grid_level = {'stable': 5, 'unstable': 6}
    for element in elements:
        levels[element] = dict()
        for block_size in all_block_sizes:
            if grid_level:
                levels[element][block_size] = [grid_level]
            else:
                levels[element][block_size] = list(range(max(1, int_log2(block_size)), max_grid_level[element] + 1))
    return levels


def navierstokes_run(args, base_name, tool):
    filter = input_filter(args.filter)

    block_size_re = re.compile(r".*block-(\d+).*")
    element_re = re.compile(r".*_(stable|unstable).*")

    application = args.type
    levels = compute_levels(args.elements, args.block_sizes, args.grid_level)

    all_args = []
    for file in os.scandir(Path(".").resolve()):
        if file.is_file() and base_name in file.name and os.access(file.path, os.X_OK):
            bs_match = block_size_re.match(file.name)
            element = element_re.match(file.name).group(1)
            block_size = int(bs_match.group(1)) if bs_match else 1
            if element in levels:
                for level in levels[element][block_size]:
                    result_file = result_map(file.path, application, level)
                    result_name = result_file.split("/")[-1]
                    if filter(result_name) and level - int_log2(block_size) >= 0:
                        all_args.append(((file.path, result_file, (application, level - int_log2(block_size), 100000)),
                                         level))
    for new_args, level in sorted(all_args, key=lambda x: x[1]):
        tool.append_args(*new_args)

    run(tool, dry_run=args.dry_run)


def post_processing(df: pd.DataFrame):
    for col in "blockSize gridLevel ilvl".split():
        df.loc[:, col] = df[col].map(lambda x: x.split("-")[1])

    for col in "dofs_u dofs_p cdofs_u cdofs_p iterations rank blockSize gridLevel float_t mem_rd mem_wr " \
               "runtime instructions vec_ratio cpi solver_iterations time_loop".split():
        df[col] = pd.to_numeric(df[col])

    for col in "runtime float_t instructions kernel_time solver_iterations mem_rd mem_wr".split():
        for app, it_col in [("operator", df.iterations), ("solver", df.time_loop)]:
            mask = df.application == app
            df.loc[mask, col] = df[col][mask] / it_col[mask]

    df.loc[:, "rank"] = df["rank"].map(lambda x: x % 20)

    return df


def process(files, args: argparse.Namespace):
    df = process_raw_regions(files, "name variant element ilvl blockSize application gridLevel rank".split(), regex,
                             "intermediate pressure projection".split())
    df = post_processing(df)
    return df


def default_column_args(*, ylabel):
    q = "\mathbb{Q}"
    elements = {"stable": rf"${q}_2$-${q}_1$", "unstable": rf"${q}_1$-${q}_1$"}
    def _default_column_args(r: str, e: str, b: int, s: pd.Series):
        args = {1: dict(marker=".", color="k"),
                2: dict(marker="1"),
                4: dict(marker="2"),
                8: dict(marker="3"),
                16: dict(marker="4"),
                32: dict(marker="x")}
        r = r.capitalize() if r else ""
        e = e.capitalize() if e else ""
        ylabel_fmt = ylabel.format(r=r, e=e, b=b) if ylabel else None
        return dict(**args[b], title=f"{elements.get(e.lower(), '')} Element", ylabel=ylabel_fmt)
    return _default_column_args


def default_legend(b: int):
    args = default_column_args(ylabel=None)("", "", b, pd.Series(dtype=object))
    args.update(label=rf"$k = {b}$" if b > 1 else "Matrix")
    return args


def local_flop(op: pd.DataFrame):
    op_t = op
    op_t.loc[:, "degree"] = 2
    op_t.loc[(op_t.region == "pressure") | (op_t.element == "unstable"), "degree"] = 1

    opm_t = to_multiindex(op_t)
    f_dof = opm_t.float_t / opm_t.index.get_level_values("dofs") / ((opm_t.degree + 1) / opm_t.degree) ** 2
    f_dof = f_dof.to_frame().query("blockSize > 1").iloc[:, 0]
    return f_dof.groupby("region element".split()).quantile(0.9)


def local_flop_upper_bound():
    index = pd.Index(["stable", "unstable"], name="element")

    def _bound(degree, bw, c):
        return ((c + 1/c + 1) * (2 * degree + 1) ** 2 - 3) * 8 * 800 / bw / (degree + 1) ** 2

    return pd.DataFrame({r'$B^i$': pd.Series([_bound(2 if e == "stable" else 1, 100, 2) for e in index], index=index),
                         r'$B^p$': pd.Series([_bound(1, 100, 1) for _ in index], index=index)})


def table_local_flop(op: pd.DataFrame, outdir: str = ""):
    fm = local_flop(op).unstack("region").drop(columns="projection").rename(lambda s: s.capitalize(), axis=1)
    fb = local_flop_upper_bound()

    q = "\mathbb{Q}"

    combined = pd.concat([fm, fb], axis=1)
    combined = combined.rename(dict(stable=rf"${q}_2$-${q}_1$", unstable=rf"${q}_1$-${q}_1$"), axis=0)
    combined.index = list(combined.index)

    print(combined)

    if outdir:
        combined.to_latex(Path(outdir) / "navier-stokes-flop-bound.tex", escape=False, float_format="{:0.2f}".format)


def visualize_dofs(op: pd.DataFrame, outdir: str):
    opm = to_multiindex(op)
    dofs = opm.index.get_level_values("dofs") / opm.runtime / 1e6
    dofs = reshape(dofs, unstack="region element blockSize".split(), keep_indices="dofs rank".split())

    fig, axs = plot_columns(apply_quantile(dofs, q=0.1), yformatter=ticker.LogFormatter(), set_size=dict(fraction=.9),
                            subplots_args=dict(sharex="none", sharey="row"),
                            default_args=dict(logx=True, logy=True, xlabel="Number of DoF"), legend=default_legend,
                            column_args=default_column_args(ylabel="{r} Step\nMDoF/s"))
    if outdir:
        fig.savefig(Path(outdir) / f"navier-stokes-dofs.pdf")


def visualize_flops(op: pd.DataFrame, outdir: str):
    opm = to_multiindex(op)

    flops_local = opm.float_t / opm.kernel_time / 1e9
    flops_local = flops_local.to_frame().query('blockSize > 1').iloc[:, 0]
    flops_local = reshape(flops_local, unstack="region element blockSize".split(), keep_indices="dofs rank".split())

    def column_args(r: str, e: str, b: int, s: pd.Series):
        if e.lower() == "stable":
            return default_column_args(ylabel="{r} Step\nLINPACK Peak (\%)")(r, e, b, s)
        else:
            return default_column_args(ylabel=None)(r, e, b, s)

    peak_flops = 910 / 20
    fig, axs = plot_columns(apply_quantile(flops_local, q=0.1), set_size=dict(fraction=.9), legend=default_legend,
                            subplots_args=dict(sharex="none", sharey="none"), column_args=column_args,
                            default_args=dict(logx=True, xlabel="Number of DoF"),
                            yformatter=ticker.PercentFormatter(xmax=peak_flops, decimals=1, symbol=""))
    if outdir:
        fig.savefig(Path(outdir) / "navier-stokes-flops.pdf")


def compare_runtime(op: pd.DataFrame):
    opm = to_multiindex(op)
    return apply_quantile(pd.concat([opm.runtime, opm.kernel_time], axis=1), q=0.9, dropna=False)


def bandwidth(op: pd.DataFrame):
    time = compare_runtime(op)
    time_transfer = time.runtime - time.kernel_time
    mask = time_transfer < 1e-15
    time_transfer.loc[mask] = time.runtime[mask]
    time_transfer = keep_multiindex(time_transfer, "region element blockSize dofs".split())

    opm = to_multiindex(op)
    mem_volume = opm.mem_rd + opm.mem_wr
    mem_volume = keep_multiindex(mem_volume, "region element blockSize dofs rank".split())
    mem_volume = mem_volume.unstack("rank").max(axis=1)

    bw = mem_volume / time_transfer
    bw.name = "bandwidth"
    return bw


def visualize_bandwidth(op: pd.DataFrame, outdir: str):
    bw = bandwidth(op)
    bw = bw.to_frame().query("dofs >= 1e4 and ((region == 'projection' and blockSize > 1) or region != 'projection')")
    bw = reshape(bw.iloc[:, 0] / 1e9, unstack="region element blockSize".split(), keep_indices="dofs")

    fig, axs = plot_columns(bw, default_args=dict(logx=True, xlabel="Number of DoF"), legend=default_legend,
                            subplots_args=dict(sharex="none", sharey="row"), set_size=dict(fraction=.9),
                            column_args=default_column_args(ylabel="{r} Step\nGiB/s"))

    if outdir:
        fig.savefig(Path(outdir) / "navier-stokes-bw.pdf")


def visualize_solver_time(sv: pd.DataFrame, outdir: str):
    svm = to_multiindex(sv)
    time = reshape(svm.runtime, unstack="region element blockSize".split(), keep_indices="dofs rank".split())
    time = apply_quantile(time, q=0.9, dropna=False)

    fig, axs = plot_columns(time, default_args=dict(logx=True, logy=True, xlabel="Number of DoF"),
                            subplots_args=dict(sharex="none", sharey="row"), legend=default_legend,
                            column_args=default_column_args(ylabel="{r} Solver\nRuntime in s"),
                            yformatter=ticker.StrMethodFormatter("{x:g}"))
    if outdir:
        fig.savefig(Path(outdir) / "navier-stokes-solve-time.pdf")

    fig, axs = plot_columns(time.apply(lambda c: c / time.index, axis=0), legend=default_legend,
                            default_args = dict(logx=True, logy=True, xlabel="Number of DoF"),
                            column_args=default_column_args(ylabel="{r} Solver\nRuntime per DoF"))
    if outdir:
        fig.savefig(Path(outdir) / "navier-stokes-solve-time-dofs.pdf")


def visualize_solver_iterations(sv: pd.DataFrame, outdir: str):
    svm = to_multiindex(sv)
    it = reshape(svm.solver_iterations, unstack="region element blockSize".split(), keep_indices="dofs rank".split())
    it = apply_quantile(it, q=0.9, dropna=False)

    fig, axs = plot_columns(it, default_args=dict(logx=True, xlabel="Number of DoF"), legend=default_legend,
                            subplots_args=dict(sharex="none", sharey="row"),
                            column_args=default_column_args(ylabel="{r} Solver\nIterations"))
    if outdir:
        fig.savefig(Path(outdir) / "navier-stokes-solve-it.pdf")


def visualize_solver_tit(sv: pd.DataFrame, outdir: str):
    svm = to_multiindex(sv)
    tit = svm.runtime / svm.solver_iterations

    mask = tit == float("inf")
    tit[mask] = svm.loc[mask, "runtime"]

    tit = reshape(tit, unstack="region element blockSize".split(), keep_indices="dofs rank".split())
    tit = apply_quantile(tit, q=0.9, dropna=False)

    fig, axs = plot_columns(tit.apply(lambda c: tit.index / c / 1e6, axis=0), legend=default_legend,
                            subplots_args=dict(sharex="none", sharey="row"),
                            yformatter=ticker.StrMethodFormatter("{x:g}"),
                            default_args=dict(logx=True, logy=True, xlabel="Number of DoF"),
                            column_args=default_column_args(ylabel="{r} Solver\nMDoF/s"))
    if outdir:
        fig.savefig(Path(outdir) / "navier-stokes-solve-it-dofs.pdf")


def to_multiindex(df: pd.DataFrame):
    return df.set_index(list(set(df.columns) & table_indices))


def merge_dofs(df: pd.DataFrame):
    df.loc[:, "dofs"] = df.dofs_u
    mask = df.region == "pressure"
    df.loc[mask, "dofs"] = df.dofs_p
    df = df.drop(columns="dofs_u dofs_p".split())
    return df


def prepare_operator(df: pd.DataFrame):
    op = df[df.application == "operator"].drop(columns="application cdofs_u cdofs_p".split())
    opm = to_multiindex(op)
    kt = opm.xs(3, level="ilvl").kernel_time
    opm = opm.xs(0, level="ilvl")
    opm.loc[:, "kernel_time"] = kt
    mask = opm.kernel_time.isna()
    opm.loc[mask, "kernel_time"] = opm.loc[mask, "runtime"]
    return opm.reset_index()


def prepare_solver(df: pd.DataFrame):
    solver = df[df.application == "solver"].drop(columns="application cdofs_u cdofs_p".split())
    solver.loc[solver.solver_iterations == 0, "solver_iterations"] = 1
    return solver


def visualize(df: pd.DataFrame, args: argparse.Namespace):
    df = merge_dofs(df)
    operator = prepare_operator(df)
    solver = prepare_solver(df)

    table_local_flop(operator, args.outdir)
    # add option to print tables?

    from multiprocessing import Pool
    with Pool() as pool:
        res = [pool.apply_async(visualize_dofs, (operator, args.outdir)),
               pool.apply_async(visualize_flops, (operator, args.outdir)),
               pool.apply_async(visualize_bandwidth, (operator, args.outdir)),
               pool.apply_async(visualize_solver_time, (solver, args.outdir)),
               pool.apply_async(visualize_solver_iterations, (solver, args.outdir)),
               pool.apply_async(visualize_solver_tit, (solver, args.outdir))]
        for r in res:
            r.get()


if __name__ == "__main__":
    base_name = "navier-stokes"

    parser, subparsers = default_parser()
    run_parser = subparsers["run"]

    run_parser.add_argument("-t", "--type", dest="type", type=str, choices=["operator", "solver"], required=True)
    run_parser.add_argument("-b", "--block-sizes", dest="block_sizes", type=int, nargs="+", default=[2, 4, 8, 16, 32],
                            choices=[2, 4, 8, 16, 32], help="Block grid size")
    run_parser.add_argument("-l", "--grid-level", dest="grid_level", type=int, help="Use fixed fine grid size")
    run_parser.add_argument("-e", "--elements", dest="elements", type=str, nargs="+", default=["stable", "unstable"],
                            choices=["stable", "unstable"], help="Finite element discretization")

    parsed_args = parser.parse_args()

    os.environ.setdefault("OMP_NUM_THREADS", "1")

    if parsed_args.mode == "run":
        used_filter = parsed_args.filter.copy()
        parsed_args.filter = used_filter + ["ilvl-0"]
        tool = PerfCounter(parsed_args.np, None, None, group="MEM_DP", instructed=True)
        navierstokes_run(parsed_args, base_name, tool)
        if parsed_args.type == "operator":
            parsed_args.filter = used_filter + ["ilvl-3"]
            tool = Pin(parsed_args.np, None, None)
            navierstokes_run(parsed_args, base_name, tool)
    elif parsed_args.mode == "process":
        default_process(parsed_args, base_name, process)
    elif parsed_args.mode == "visualize":
        default_visualize(parsed_args, visualize)
    else:
        parser.print_help()
