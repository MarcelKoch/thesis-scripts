#! /usr/bin/env python3
from functools import partial
from main import *
from matplotlib.ticker import *
from plotting import *
from profilers import *
from processing import *


total_fp_re = dict((re.compile(r"\|\s+{}\s+\|\s+\S+\s+\|\s+(?P<float_t>\d+)".format(k)), v)
                   for k, v in {"FP_ARITH_INST_RETIRED_SCALAR_DOUBLE": 1,
                                "FP_ARITH_INST_RETIRED_128B_PACKED_DOUBLE": 2,
                                "FP_ARITH_INST_RETIRED_256B_PACKED_DOUBLE": 4,
                                "FP_ARITH_INST_RETIRED_512B_PACKED_DOUBLE": 8}.items())

dof_def_re = re.compile(r"DOF\s+(?P<dofs>\d+)")
it_def_re = re.compile(r"IT\s+(?P<iterations>\d+)")
runtime_def_re = re.compile(r"TIME\s+(\d[^\d]+)?(?P<runtime>\d+(\.\d+(e[+-]\d+)?))")


def _flop_scaling(flops, simd_width):
    return float(flops) * simd_width


regex = [dof_def_re, it_def_re, runtime_def_re] + \
         [(k, partial(_flop_scaling, simd_width=v)) for k, v in total_fp_re.items()]

table_indices = set("model variant mode dim grid blockSize rank dofs".split())


def result_map(execfile: str, p: int):
    return f"{execfile.replace('src/', 'results/')}_{p}"


def call_args(_: str):
    return tuple()


def post_processing(df: pd.DataFrame):
    for col in "blockSize".split():
        df.loc[:, col] = df[col].map(lambda x: x.split("-")[1])

    for col in "dofs rank blockSize".split():
        df[col] = pd.to_numeric(df[col])

    df.loc[:, "runtime"] = df.runtime / df.iterations
    df.loc[:, "float_t"] = df.float_t / df.iterations

    df = df.drop(columns="iterations")

    df.loc[:, "rank"] = df["rank"].map(lambda x: x % 20)

    return df


def process(files, args: argparse.Namespace):
    df = process_raw(files, "model mode grid blockSize rank".split(), regex)
    df = post_processing(df)
    return df


def runtime_fraction(df: pd.DataFrame):
    df = df.drop(columns="model dofs".split())
    dfm = to_multiindex(df)
    rt = dfm.runtime
    rt = apply_quantile(rt)

    rt = rt.unstack("mode blockSize".split())
    rt = rt.apply(lambda c: c / rt[("default", 1)])

    rt = rt.drop(columns="default")
    rt = rt.stack("mode")
    rt = rt.T
    return rt


def plot_runtime(rt: pd.DataFrame):
    def column_args(geo: str, mode: str, s: pd.Series):
        label = {"bsg": "Without Optimizations", "opt": "With Optimizations"}.get(mode, None)
        return dict(title=f"{geo.capitalize()} Geometry" if geo else None, label=label)

    fig, axs = plot_columns(rt * 100, set_size=True, legend=False, subplots_args=dict(sharey="all", sharex="all"),
                            default_args=dict(logx=True, ylabel="Kernel Runtime (\%)", xlabel="Block Size"),
                            column_args=column_args, yformatter=PercentFormatter(symbol=""))
    for ax in axs.flatten():
        ax.axhline(100, **plot_format["guideline"])
    axs[0, 1].legend()
    fig.savefig(base_path / "img/geo.pdf", format="pdf", transparent=True, bbox_inches="tight")


def table_flop(float_t: pd.Series):
    p = pivot(float_t, "grid mode".split())
    p = p.float_t
    p.loc[:, ("affine", "default")] = p.loc[1, ("affine", "default")]
    p.loc[:, ("multilinear", "default")] = p.loc[1, ("multilinear", "default")]
    p = p.loc[32, :].unstack("mode")
    p = p["default bsg opt".split()]  # reorder columns
    p = p.rename(columns={"default": "Default", "bsg": "Blockstructured", "opt": "Optimized Blockstructured"},
                 index=str.capitalize)
    p = p.rename_axis(columns=lambda s: "", index=lambda s: "")
    p = p.reset_index()
    p.to_latex(base_path / "tbl/geo-trafo-flop.tex", index=False)


def visualize(df: pd.DataFrame, args: argparse.Namespace):
    rt = runtime_fraction(df)
    plot_runtime(rt)
    #table_flop(apply_quantile(to_multiindex(df).float_t))


def to_multiindex(df: pd.DataFrame):
    return df.set_index(list(table_indices & set(df.columns)))


if __name__ == "__main__":
    base_name = "geometry-transformation"

    parser, _ = default_parser()
    parsed_args = parser.parse_args()

    tool = PerfCounter(parsed_args.np, call_args, result_map, group="FLOPS_DP", instructed=False)

    main(base_name=base_name, tool=tool, process=process, visualize=visualize, parser=parser, parsed_args=parsed_args)
