#! /usr/bin/env python3
from vectorization import *


def process(files, args: argparse.Namespace):
    df = process_raw(files, "model variant dim vecType simdSize blockSize quadOrder rank".split(), regex)
    df = post_processing(df)
    return df


def plot_speedup(sp: pd.DataFrame):
    fig, axs = plt.subplots(2, 2, squeeze=False, sharey="all", sharex="none")
    for i, dim in enumerate([2, 3]):
        spd = sp.xs(dim, level="dim")
        permute = reshape(spd.permute, unstack="quadOrder", keep_indices="blockSize")
        simple = reshape(spd.simple, unstack="quadOrder", keep_indices="blockSize")

        for ax, vec, title in zip(axs[i].flatten(), [simple, permute], ["Simple Vectorization", "Permute Vectorization"]):
            ax.axhline(4, **plot_format["guideline"])
            ax.axhline(8, **plot_format["guideline"])
            ax.set_prop_cycle(None)  # reset color cylce
            vec[4].plot(logx=True, marker="v", ls=":", ax=ax, legend=False, label=None)
            ax.set_prop_cycle(None)
            vec[8].plot(logx=True, marker="^", ls="-.", ax=ax, legend=False, label=None)
            if dim == 2:
                ax.set_title(title)
                ax.set_xlabel(None)
            else:
                ax.set_xlabel("Block Size")

        axs[i, 0].set_ylabel(f"Speed-Up {dim}D")

    fig, axs = set_legend(fig, axs, simple, lambda qo: dict(label=rf"$q = {qo}$"), set_size=True)

    fig.savefig(base_path / "img/gen-vectorization-speedup.pdf")


def visualize(df: pd.DataFrame, args: argparse.Namespace):
    df.loc[:, "dim"] = pd.to_numeric(df.dim.apply(lambda s: s.split("-")[1]))
    dfs = speedup(df)
    plot_speedup(dfs)


if __name__ == "__main__":
    base_name = "vectorization"

    parser, _ = default_parser()
    parsed_args = parser.parse_args()

    tool = PerfCounter(parsed_args.np, call_args, result_map, group="FLOPS_DP", instructed=False)

    main(base_name=base_name, tool=tool, process=process, visualize=visualize, parser=parser, parsed_args=parsed_args)
