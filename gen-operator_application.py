#! /usr/bin/env python3
from operator_application import *


def process(files, args: argparse.Namespace):
    df = process_raw(files, "model variant dim blockSize quadOrder rank".split(), regex)
    df = post_processing(df)
    df.loc[:, "dim"] = pd.to_numeric(df.dim.apply(lambda s: s.split("-")[1]))
    return df


def dofs(dfm: pd.DataFrame):
    dofs = dfm.index.get_level_values("dofs") / dfm.runtime
    dofs = apply_quantile(dofs, q=0.1) / 1e6
    dofs = reshape(dofs, unstack="dim variant quadOrder".split(), keep_indices="blockSize")
    return dofs


def flops(dfm: pd.DataFrame):
    flops = dfm.float_t / dfm.local_runtime
    flops = apply_quantile(flops, q=0.1) / 1e9
    flops = reshape(flops, unstack="dim variant quadOrder".split(), keep_indices="blockSize")
    return flops


def visualize_dofs(dfm: pd.DataFrame, outdir: str = ""):
    dofs_ = dofs(dfm)
    dofs_2d = dofs_[2]
    dofs_3d = dofs_[3]

    matrix_dofs = {"2D": 100 / (2 * (3**2) * 8) * 1e3 / 20,
                   "3D": 100 / (2 * (3**3) * 8) * 1e3 / 20}

    fig, axs = plt.subplots(1, 2, sharex="none", sharey="none", squeeze=False)
    for ax, d, title in zip(axs.flatten(), [dofs_2d, dofs_3d], ["2D", "3D"]):
        ax.set_prop_cycle(None)  # reset color cylce
        d["generated"].plot(marker="v", ls="-.", ax=ax, legend=False, label=None, title=title, xlabel="Block Size",
                            logx=True, logy=True)
        ax.set_prop_cycle(None)
        if "handwritten" in d.columns:
            d["handwritten"].plot(marker="^", ls=":", ax=ax, legend=False, label=None, xlabel="Block Size")
        ax.axhline(matrix_dofs[title], **plot_format["guideline"])
    axs[0, 0].set_ylabel("MDoF/s")
    axs[0, 0].yaxis.set_major_formatter(StrMethodFormatter("{x:g}"))
    axs[0, 1].yaxis.set_major_formatter(StrMethodFormatter("{x:g}"))
    fig, axs = set_legend(fig, axs, d, lambda qo: dict(label=rf"$q = {qo}$"), set_size=True)
    if outdir:
        fig.savefig(Path(outdir) / "gen-operator-application-dofs.pdf")


def visualize_flops(dfm: pd.DataFrame, outdir: str = ""):
    flops_ = flops(dfm)
    flops_2d = flops_[2]
    flops_3d = flops_[3]

    peak_flops = 910 / 20

    fig, axs = plt.subplots(1, 2, sharex="none", sharey="row", squeeze=False)
    for ax, d, title in zip(axs.flatten(), [flops_2d, flops_3d], ["2D", "3D"]):
        ax.set_prop_cycle(None)  # reset color cylce
        d["generated"].plot(marker="v", ls="-.", ax=ax, legend=False, label=None, title=title,
                            xlabel="Block Size", logx=True)
        ax.set_prop_cycle(None)
        if "handwritten" in d.columns:
            d["handwritten"].plot(marker="^", ls=":", ax=ax, legend=False, label=None, xlabel="Block Size")
        ax.get_yaxis().set_major_formatter(PercentFormatter(xmax=peak_flops, symbol="", decimals=0))
        ax.get_yaxis().set_minor_formatter(PercentFormatter(xmax=peak_flops, symbol="", decimals=0))
        ax.axhline(peak_flops, **plot_format["guideline"])
    axs[0, 0].set_ylabel("LINPACK Peak (\%)")
    fig, axs = set_legend(fig, axs, d, lambda qo: dict(label=rf"$q = {qo}$"), set_size=True)
    if outdir:
        fig.savefig(Path(outdir) / "gen-operator-application-flops.pdf")


def visualize(df: pd.DataFrame, args: argparse.Namespace):
    dfm = prepare(df)
    visualize_flops(dfm, args.outdir)
    visualize_dofs(dfm, args.outdir)


if __name__ == "__main__":
    base_name = "operator-application"

    parser, _ = default_parser()
    parsed_args = parser.parse_args()

    tool = PerfCounter(parsed_args.np, call_args, result_map, group="FLOPS_DP", instructed=False)

    main(base_name=base_name, tool=tool, process=process, visualize=visualize, parser=parser, parsed_args=parsed_args)
