import os
import re
import shlex
import subprocess
from collections import OrderedDict
from pathlib import Path


class Tool(object):

    def __init__(self, call_args, result_map):
        self.call_args = call_args
        self.result_map = result_map
        self.args_list = []

    def print(self, args, *, debug=False, prefix=" " * 4):
        if not debug:
            for c in OrderedDict.fromkeys([(Path(c[0]).parts[-1], ":") + c[min(3, len(c)):] for c in args]):
                print(prefix + c)
        else:
            for c in args:
                print(c)

    def append_args(self, execfile):
        raise NotImplementedError()

    @staticmethod
    def run(args):
        raise NotImplementedError()


class SDE(Tool):
    cmd = "/home/marckoch/intel/sde/sde64 -d -iform 1 -skl"
    mark_regex = re.compile(r"Timed\sregion\s(\w+):\s(\d+)\s<-->\s(\d+)")
    GUESS_MARKS = True
    name = 'sde'

    def __init__(self, call_args, result_map):
        super().__init__(call_args, result_map)
        self.args_list = [[]]

    def get_sde_marks(self, execfile):
        if SDE.GUESS_MARKS:
            marks = {'residual_evaluation': (22, 33),
                     'apply_jacobian': (44, 55) if 'q1' not in execfile else (66, 77)}
        else:
            cp = subprocess.run((execfile,) + self.call_args(execfile), stdout=subprocess.PIPE, universal_newlines=True)
            marks = dict()
            for l in cp.stdout.splitlines():
                match = SDE.mark_regex.match(l)
                if match:
                    marks[match.group(1)] = (match.group(2), match.group(3))
            del marks['driver']
        return marks

    def append_args(self, execfile):
        marks = self.get_sde_marks(execfile)
        for kernel in marks:
            self.args_list[0].append((execfile, {kernel: marks[kernel]}, self.call_args(execfile)))

    @staticmethod
    def run(args):
        execfile, marks, call_args = args
        for kernel in marks:
            outfile = execfile.replace('sde', kernel).replace('benchmarks/models', 'results/sde') + "_0.txt"
            os.makedirs(outfile[:outfile.rindex('/')], exist_ok=True)
            subprocess.run(shlex.split(SDE.cmd) +
                           shlex.split("-omix {}".format(outfile)) +
                           shlex.split("-start_ssc_mark {} -stop_ssc_mark {}".format(*marks[kernel])) +
                           ["--"] + [execfile] + call_args, stdout=subprocess.DEVNULL, universal_newlines=True)


class NOPIN(Tool):
    def __init__(self, call_args=None, result_map=None):
        super().__init__(call_args, result_map)
        self.args_list = [[]]

    def append_args(self, execfile, outfile=None, extra_args=()):
        outfile = outfile or self.result_map(execfile)
        extra_args = extra_args or self.call_args(execfile)

        self.args_list[0].append((execfile, 0, outfile) + tuple(map(str, extra_args)))

    @staticmethod
    def run(args):
        execfile, _, outfile, *call_args = args
        os.makedirs(outfile[:outfile.rindex('/')], exist_ok=True)
        try:
            cp = subprocess.run([execfile] + list(call_args), stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                universal_newlines=True, check=True)
        except subprocess.CalledProcessError as e:
            parts = outfile.split("/")
            logfile = f"{'/'.join(parts[:-1])}/log_{parts[-1]}"
            with open(logfile, "a") as f:
                f.write(e.stderr)
                f.write(e.stdout)
            raise e
        else:
            with open(outfile, "w") as f:
                f.write(cp.stdout)


class MPI(Tool):
    def __init__(self, np, call_args, result_map):
        super().__init__(call_args, result_map)
        self.np = np
        self.even = True
        self.args_list = []

    def command(self, i, execfile, outfile=None, extra_args=()):
        raise NotImplementedError()

    def print(self, args, *, debug=False, prefix=" " * 4):
        def _simplfy_path(f):
            if "/" in f and not debug:
                return f.split("/")[-1]
            else:
                return f

        for l in args:
            cmds = [f"{' '.join(map(_simplfy_path, c.split()))} > {_simplfy_path(o)}" for c, o in l]
            cmds = cmds[:1] if not debug else cmds
            for cmd in cmds:
                print(prefix + cmd)

    def append_args(self, execfile, outfile=None, extra_args=()):
        if self.even:
            process_range = range(self.np)
        else:
            process_range = range(self.np, 2 * self.np)

        new_args = [[self.command(i, execfile, outfile, extra_args) for i in process_range]]
        if self.even:
            self.args_list.append(new_args)
        else:
            self.args_list[-1] += new_args
        self.even = not self.even

    def run(self, args):
        outdir, = set("/".join(o.split("/")[:-1]) for _, o in args)
        os.makedirs(outdir, exist_ok=True)
        try:
            cmds = [f"-np 1 wrapper {o} {c}" for c, o in args]
            subprocess.run(shlex.split("mpirun --bind-to none " + " : ".join(cmds)),
                           stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, universal_newlines=True)
        except subprocess.CalledProcessError as e:
            with open(f"{outdir}/log_{os.getpid()}", "a") as f:
                f.write(e.stderr)
                f.write(e.stdout)
            raise


class PerfCounter(MPI):
    cmd = "likwid-perfctr -f"
    name = 'likwid'

    def __init__(self, np, call_args, result_map, *, group, instructed):
        super().__init__(np, call_args, result_map)
        self.group = group
        if instructed:
            self.cmd = f"{PerfCounter.cmd} -m"

    def command(self, i, execfile, outfile=None, extra_args=()):
        if self.group == "MEM_DP":
            if i % self.np == 0:
                group = "MEM_DP"
            else:
                group = "FLOPS_DP"
        else:
            group = self.group

        outfile_map = lambda i: f"{outfile}_{i}" if outfile else self.result_map(execfile, i)
        extra_args = extra_args or self.call_args(execfile)

        return (f"{self.cmd} -g {group} -C {i} {execfile} {' '.join(map(str, extra_args))}", outfile_map(i))


class Pin(MPI):
    cmd = "likwid-pin -q"
    name = 'pin'

    def command(self, i, execfile, outfile=None, extra_args=()):
        outfile_map = lambda i: f"{outfile}_{i}" if outfile else self.result_map(execfile, i)
        extra_args = extra_args or self.call_args(execfile)

        return (f"{self.cmd} -c {i} {execfile} {' '.join(map(str, extra_args))}", outfile_map(i))
