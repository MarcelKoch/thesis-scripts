#! /usr/bin/env python3
from functools import partial

from profilers import *
from main import *
from plotting import *
from processing import *


vec_ratio_re = re.compile(r"\|\s+Vectorization ratio\s+\|\s+(?P<vec_ratio>\d+(\.\d+(e[+-]\d+)?)?)")
total_fp_re = dict((re.compile(r"\|\s+{}\s+\|\s+\S+\s+\|\s+(?P<float_t>\d+)".format(k)), v)
                   for k, v in {"FP_ARITH_INST_RETIRED_SCALAR_DOUBLE": 1,
                                "FP_ARITH_INST_RETIRED_128B_PACKED_DOUBLE": 2,
                                "FP_ARITH_INST_RETIRED_256B_PACKED_DOUBLE": 4,
                                "FP_ARITH_INST_RETIRED_512B_PACKED_DOUBLE": 8}.items())

dof_def_re = re.compile(r"DOF\s+(?P<dofs>\d+)")
it_def_re = re.compile(r"IT\s+(?P<iterations>\d+)")
runtime_def_re = re.compile(r"TIME\s+(\d[^\d]+)?(?P<runtime>\d+(\.\d+(e[+-]\d+)?))")


def _flop_scaling(flops, simd_width):
    return float(flops) * simd_width


regex = [dof_def_re, it_def_re, runtime_def_re, vec_ratio_re] + \
         [(k, partial(_flop_scaling, simd_width=v)) for k, v in total_fp_re.items()]

table_indices = set("model variant dim vecType simdSize blockSize quadOrder rank dofs".split())


def result_map(execfile: str, p: int):
    return f"{execfile.replace('src/', 'results/')}_{p}"


def call_args(_: str):
    return tuple()


def post_processing(df: pd.DataFrame):
    for col in "simdSize blockSize quadOrder".split():
        df.loc[:, col] = df[col].map(lambda x: x.split("-")[1])

    for col in "dofs rank simdSize blockSize quadOrder".split():
        df[col] = pd.to_numeric(df[col])

    df.loc[:, "runtime"] = df.runtime / df.iterations
    df.loc[:, "float_t"] = df.float_t / df.iterations

    df = df.drop(columns="iterations")

    df.loc[:, "rank"] = df["rank"].map(lambda x: x % 20)

    df.loc[:, "vecType"] = df["vecType"].map(lambda x: x.split("-")[1] if x != "default" else x)

    df.loc[df.simdSize == 1, "vecType"] = "default"
    df.loc[df.vecType == "default", "simdSize"] = 1

    return df


def process(files, args: argparse.Namespace):
    df = process_raw(files, "model vecType simdSize blockSize quadOrder rank".split(), regex)
    df = post_processing(df)
    return df


def compare_default(df: pd.DataFrame, val: str):
    dfm = to_multiindex(df)

    c = apply_quantile(dfm[val])
    c = c.unstack("vecType simdSize".split())

    def_ft = c.loc[:, idx["default", 1]]
    c = c.apply(lambda c: def_ft / c, axis=0)
    return c


def float_t(df: pd.DataFrame):
    return compare_default(df, "float_t")


def speedup(df: pd.DataFrame):
    return compare_default(df, "runtime")


def plot_speedup(sp: pd.DataFrame):
    permute = reshape(sp.permute, unstack="quadOrder", keep_indices="blockSize")
    simple = reshape(sp.simple, unstack="quadOrder", keep_indices="blockSize")

    fig, axs = plt.subplots(1, 2, figsize=latex_figsize(), squeeze=False)
    for ax, vec, title in zip(axs.flatten(), [simple, permute], ["Simple Vectorization", "Permute Vectorization"]):
        ax.axhline(4, **plot_format["guideline"])
        ax.axhline(8, **plot_format["guideline"])
        ax.set_prop_cycle(None)  # reset color cylce
        vec[4].plot(logx=True, marker="v", ls=":", ax=ax, legend=False, label=None)
        ax.set_prop_cycle(None)
        vec[8].plot(logx=True, marker="^", ls="-.", ax=ax, legend=False, label=None, title=title, xlabel="Block Size")

    axs[0, 0].set_ylabel("Speed-Up")

    fig, axs = set_legend(fig, axs, simple, lambda qo: dict(label=rf"$q = {qo}$"), set_size=False)

    fig.savefig(base_path / "img/vectorization-speedup.pdf")


def table_vec_ratio(df_vec: pd.DataFrame, df_def: pd.DataFrame):
    dfs = {"vec": df_vec, "def": df_def}
    for k in dfs:
        dfs[k] = dfs[k].drop(columns="dofs model runtime float_t".split())

        mask = dfs[k].vecType == "default"
        dfs[k].loc[mask, "simdSize"] = ""

        dfs[k] = apply_quantile(dfs[k], values="vec_ratio")
        dfs[k] = dfs[k].vec_ratio

    idx = pd.IndexSlice
    df = dfs["vec"]
    df.loc[idx["default"], :] = dfs["def"]

    df = df.unstack("vecType simdSize".split())
    df = df.loc[idx[:, (2, 14)], :]

    df = df.rename(columns={"default": "None", "permute": "Permute", "simple": "Simple"})
    df = df.rename_axis(columns={"vecType": "Vectorization", "simdSize": "SIMD Width"},
                        index={"blockSize": r"$k$", "quadOrder": r"$q$"})
    df = df.unstack(0).stack(-1)
    df.to_latex(base_path / "tbl/vec-ratio.tex", escape=False, multirow=True, sparsify=True)


def visualize(df: pd.DataFrame, args: argparse.Namespace):
    if "iterations" in df.columns:
        df = df.drop(columns="iterations")
    dfs = speedup(df)
    plot_speedup(dfs)
    print(dfs)
    if args.vec_ratio:
        dfvr = pd.read_csv(args.vec_ratio)
        table_vec_ratio(df, dfvr)


def to_multiindex(df: pd.DataFrame):
    return df.set_index(list(table_indices & set(df.columns)))


if __name__ == "__main__":
    parser, subparsers = default_parser()
    visualize_parser = subparsers["visualize"]
    visualize_parser.add_argument("--vec-ratio", dest="vec_ratio", type=str)

    base_name = "vectorization"

    parser, _ = default_parser()
    parsed_args = parser.parse_args()

    tool = PerfCounter(parsed_args.np, call_args, result_map, group="FLOPS_DP", instructed=False)

    main(base_name=base_name, tool=tool, process=process, visualize=visualize, parser=parser, parsed_args=parsed_args)
