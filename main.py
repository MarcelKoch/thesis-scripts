import multiprocessing
import argparse
import os
import shlex
import subprocess

import pandas as pd
from pathlib import Path

from filtering import input_filter
from processing import to_file, simplify


def run(tool, *, dry_run=False):
    if not dry_run:
        subprocess.run(shlex.split("likwid-setFrequencies -t 0 -g performance"), check=True)
    if tool.args_list:
        try:
            with multiprocessing.Pool(min(multiprocessing.cpu_count() // 2, len(tool.args_list[0]))) as pool:
                for l in tool.args_list:
                    print("-" * 80)
                    print("Now running:")
                    tool.print(l)
                    if not dry_run:
                        pool.map(tool.run, l)
        except Exception:
            raise
        finally:
            if not dry_run:
                subprocess.run(shlex.split("likwid-setFrequencies -t 1 -g ondemand"))


def default_parser():
    parser = argparse.ArgumentParser()
    parser.set_defaults(mode=None)
    subparsers = parser.add_subparsers()

    parser_run = subparsers.add_parser("run", aliases=["r"], help="Run benchmarks.")
    parser_run.set_defaults(mode="run")
    parser_run.add_argument('-np', '--nproc', dest='np', default=20, type=int,
                            help="Number of max parallel threads per socket.")
    parser_run.add_argument('-f', '--filter', dest='filter', type=str, nargs='+', default=[], help="Filter benchmarks.")
    parser_run.add_argument('--dry-run', dest='dry_run', action="store_true", help="Dry run.")

    parser_process = subparsers.add_parser("process", aliases=["p"], help="Process benchmarks.")
    parser_process.set_defaults(mode="process")
    parser_process.add_argument('-f', '--filter', dest='filter', type=str, nargs='+', help="Filter benchmarks.")
    parser_process.add_argument('-o', '--outfile', dest='outfile', default="test", type=str,
                                help="Name of the output file.")
    parser_process.add_argument('-s', '--simplify', dest='simplify', action="store_true",
                                help="Simplify processed data.")

    parser_visualize = subparsers.add_parser("visualize", aliases=["v"], help="Visualize processed data.")
    parser_visualize.set_defaults(mode="visualize")
    parser_visualize.add_argument('-i', '--infile', dest='infile', default="test", type=str,
                                  help="Name of the input file.")
    parser_visualize.add_argument('-d', '--dir', dest='outdir', default="test", type=str, required=True,
                                  help="Name of the output directory.")

    subparser_dict = {"run": parser_run, "process": parser_process, "visualize": parser_visualize}

    return parser, subparser_dict


def default_run(args, base_name, tool):
    filter = input_filter(args.filter)

    for file in os.scandir(Path(".").resolve()):
        if file.is_file() and base_name in file.name and os.access(file.path, os.X_OK) and filter(file.name):
            tool.append_args(file.path)

    run(tool, dry_run=args.dry_run)


def default_process(args, base_name, process):
    filter = input_filter(args.filter)

    to_process = []
    for file in os.scandir(Path(".")):
        if (file.is_file() and base_name in file.name and os.access(file.path, os.R_OK)
                and not file.name.endswith(".gz") and filter(file.name)):
            to_process.append(file.path)

    df = process(to_process, args)
    to_file(df, args.outfile, compress=True, timestamp=True)
    if args.simplify:
        df_simplify = simplify(df, "runtime")
        to_file(df_simplify, f"{args.outfile}_simplified", compress=False, timestamp=True)


def default_visualize(args, visualize):
    file, *_ = sorted((f for f in os.scandir(".") if f.is_file() and (f.name.startswith(args.infile) or
                                                                      f.name == args.infile) and
                       ".csv" in f.name and "simplified" not in f.name), reverse=True, key=os.path.getmtime)
    df = pd.read_csv(file)
    visualize(df, args)


def main(*, base_name, tool, process=None, visualize=None, parser=None, parsed_args=None):
    if not parser:
        parser, _ = default_parser()
    if not parsed_args:
        parsed_args = parser.parse_args()

    os.environ.setdefault("OMP_NUM_THREADS", "1")

    if parsed_args.mode == "run":
        default_run(parsed_args, base_name, tool)
    elif parsed_args.mode == "process":
        if process:
            default_process(parsed_args, base_name, process)
    elif parsed_args.mode == "visualize":
        if visualize:
            default_visualize(parsed_args, visualize)
    else:
        parser.print_help()
