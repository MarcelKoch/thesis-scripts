#! /usr/bin/env python3

from plotting import *
from matplotlib.text import Text
import numpy as np


class Configuration(object):
    def __init__(self, l1_bandwidth=None, l2_bandwidth=None, l3_bandwidth=None, mem_bandwidth=None,
                 flop_scalar=None, flop_avx2=None, flop_avx2fma=None, flop_avx512=None, flop_avx512fma=None,
                 flop_peak=None):
        bandwidth = {'L1': l1_bandwidth,
                     'L2': l2_bandwidth,
                     'L3': l3_bandwidth,
                     'MEM': mem_bandwidth}
        self.bandwidth = dict()
        for k, v in bandwidth.items():
            if v:
                if isinstance(v, tuple):
                    self.bandwidth[k] = v
                else:
                    self.bandwidth[k] = (v, dict())
        self.max_bandwidth = max([v[0] for v in self.bandwidth.values()])

        flops = {'scalar': flop_scalar,
                 'avx2': flop_avx2,
                 'avx2fma': flop_avx2fma,
                 'avx512': flop_avx512,
                 'avx512fma': flop_avx512fma,
                 'Peak': flop_peak}
        self.flops = dict()
        for k, v in flops.items():
            if v:
                if isinstance(v, tuple):
                    self.flops[k] = v
                else:
                    self.flops[k] = (v, dict())
        self.max_flop = max([v[0] for v in self.flops.values()])


def plot_roofs(ax, xrange, yrange, conf, basex=2, title=''):
    frame_size = ax.figure.get_size_inches()
    rot = np.degrees(np.arctan(frame_size[1]/frame_size[0]))  # angle of the line y=x

    for label, (v, props) in conf.bandwidth.items():
        bound = lambda x: x * v
        inv = lambda x: x / v

        if 'label' in props:
            label = props['label']

        xs = [xrange[0], min(xrange[1], inv(conf.max_flop))]
        ax.plot(xs, [bound(x) for x in xs], solid_capstyle="butt",)
        t = Text(text=label, x=xrange[0], y=bound(xrange[0]) * 0.85, va="top", ha="left",
                 rotation=rot, rotation_mode='anchor', **props)
        ax.add_artist(t)

    for label, (v, props) in conf.flops.items():
        bound = lambda x: v
        inv = lambda x: x / conf.max_bandwidth
        xs = [max(inv(v), xrange[0]), xrange[1]]

        if 'label' in props:
            label = props['label']

        ax.plot(xs, [bound(x) for x in xs], solid_capstyle="round",)
        t = Text(text=label, x=xrange[1], y=bound(xrange[1]) * 0.9, va="top", ha="right", **props)
        ax.add_artist(t)

    ax.axis(xrange + yrange)
    ax.grid(True, 'major', linestyle='-')
    ax.grid(True, 'minor', linestyle='-', alpha=0.75)

    ax.set_xscale('log', base=basex)
    ax.set_yscale('log', base=10)
    from matplotlib.ticker import StrMethodFormatter, LogLocator, ScalarFormatter
    ax.xaxis.set_major_formatter(StrMethodFormatter("{x:g}"))
    ax.yaxis.set_major_formatter(StrMethodFormatter("{x:g}"))

    ax.set_xlabel('FLOP/Byte (AI)')
    ax.set_ylabel('GFLOP/s')
    ax.set_title(title)


def plot_oi(ax, oi, yrange, conf, title='', **kwargs):
    try:
        line, = ax.plot([oi, oi], [yrange[0], min(conf.max_bandwidth * oi, conf.max_flop)], linestyle='-', color='grey',
                       solid_capstyle="round")
        if title != '':
            properties = dict(rotation='vertical', verticalalignment='bottom')
            properties.update(kwargs)
            ax.annotate(title, (oi, yrange[0]), **properties)
        return line
    except TypeError:
        return ax.scatter([oi], [yrange], color='red')


if __name__ == "__main__":
    xrange = [0.01, 100]
    yrange = [0.01, 200]

    fig, ax = plt.subplots()
    skylake = Configuration(mem_bandwidth=100 / 20, l1_bandwidth=4360 / 20, l2_bandwidth=1340 / 20,
                            l3_bandwidth=365 / 20, flop_peak=1408 / 20)
    plot_roofs(ax, xrange, yrange, skylake, basex=2)
    fig = set_latex_size(fig, ax,)
    line1 = plot_oi(ax, 1/8, yrange, skylake, 'SpMxV')
    line2 = plot_oi(ax, 1/12, yrange, skylake,)
    ydata1 = line1.get_ydata()
    ydata2 = line2.get_ydata()
    ax.fill_betweenx([ydata1[0], ydata2[-1], ydata1[-1]], 1/8, [1/12, 1/12, 1/8], alpha=0.5)
    plt.savefig('roofline.pdf')
