from collections import Iterable, Mapping
from typing import Union, Callable

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path


try:
    plt.style.use("thesis")
except:
    pass

import matplotlib
from matplotlib.backends.backend_pgf import FigureCanvasPgf
#matplotlib.backend_bases.register_backend('pdf', FigureCanvasPgf)
matplotlib.use("pgf")

base_path = Path.home() / 'Documents/thesis'
textwidth = 5.2503  # latex's \textwidth in in
textheight = 7.931  # latex's \textheight in in

plot_format = {"guideline": dict(ls="--", c="grey", alpha=0.75)}


def latex_figsize(ax=None, *, subplots=None, fraction=1., spread=False, transposed=False):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    ax: matplotlib.axes.Axes
            Axes object which ratios should be adjusted
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    spread: bool
            use whole text height for full page plot
    transposed: bool
            use the y-axis as the long side
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches

    inspired by https://jwalton.info/Embed-Publication-Matplotlib-Latex/
    """
    ratio = (1 + 5**.5) / 2
    if spread:
        if fraction < 1.:
            print(f"Ignoring fraction ({fraction}) if spread is set")
        return textwidth, textheight * 0.85
    elif ax:
        bbox = ax.get_position()
        x = bbox.x1 - bbox.x0
        y = bbox.y1 - bbox.y0

        w = textwidth * fraction
        if transposed:
            return w, ratio * w * (x / y)
        else:
            return w, w * (x / y) / ratio
    else:
        if subplots is None:
            subplots = 1, 1
        return textwidth, textwidth / ratio * (subplots[0] / subplots[1])


def plot_columns(df: pd.DataFrame, *, axs=None, fig=None, yformatter=None, yformatter_minor=None, set_size=True,
                 subplots_args=None, legend=None, column_args=None, default_args=None):
    if default_args is None:
        default_args = dict()
    if subplots_args is None:
        subplots_args = dict(sharex="none", sharey="row")
    if column_args is None:
        column_args = lambda *args: dict()
    if set_size is True:
        set_size = dict()
    if isinstance(df.columns, pd.MultiIndex):
        if len(df.columns.levels) > 3:
            raise RuntimeError("Too many levels to create 2d subplot grid")
        cols = len(df.columns.levels[-2])
        rows = len(df.columns.levels[0]) if len(df.columns.levels) == 3 else 1
    else:
        cols = 1
        rows = 1
    if axs is None:
        assert fig is None
        fig, axs = plt.subplots(rows, cols, squeeze=False, **subplots_args)

    def _do_plot(_df: pd.DataFrame, *i, current_column=None):
        if current_column is None:
            current_column = tuple()
        if isinstance(_df.columns, pd.MultiIndex):
            for k, c in enumerate(sorted(_df.columns.levels[0])):
                try:
                    _do_plot(_df[c], *i, k, current_column=current_column + (c,))
                except KeyError:
                    pass
        else:
            if len(i) < 1:
                i = (0, 0)
            if len(i) < 2:
                i = (0,) + i
            for c in sorted(_df.columns):
                s = _df[c].dropna()
                plot_args = column_args(*current_column, c, s)
                plot_args.update(**default_args)
                s.plot(ax=axs[i], **plot_args)
            if yformatter:
                axs[i].get_yaxis().set_major_formatter(yformatter)
            if yformatter_minor:
                axs[i].get_yaxis().set_minor_formatter(yformatter_minor)
            if i[0] > 0:
                axs[i].set_title(None)
            if i[0] < rows - 1:
                axs[i].set_xlabel(None)
    _do_plot(df)

    if legend:
        fig, axs = set_legend(fig, axs, df, legend, set_size=set_size)

    if isinstance(set_size, Mapping):
        fig = set_latex_size(fig, axs.flatten()[0], **set_size)

    return fig, axs


def set_latex_size(fig, ax, **kwargs):
    def _set_size():
        fig.execute_constrained_layout()
        fig.set_size_inches(latex_figsize(ax, **kwargs))
        fig.set_constrained_layout(False)
    _set_size()
    _set_size()  # somehow applying it twice results in better gaps if subplots with muiltiple rows is used
    return fig


def determine_legends(ax, df, legend_args: Union[str, Mapping, Callable]):
    if not callable(legend_args):
        legend_args_v = legend_args
        if not isinstance(legend_args_v, Mapping):
            legend_args_v = dict(label=legend_args_v)

        def legend_args(c):
            return legend_args_v

    ax.set_prop_cycle(None)  # reset color cylce
    legends = []
    lowest_level_df = df
    while isinstance(lowest_level_df.columns, pd.MultiIndex):
        lowest_level_df = lowest_level_df[lowest_level_df.columns.levels[0][0]]
    valid_keys = {"marker", "c", "color", "ls", "linestyle", "label"}
    for c in sorted(lowest_level_df.columns):
        legends.append(ax.plot([], [], **{k: v for k, v in legend_args(c).items()
                                          if k in valid_keys})[0])
    return legends


def set_legend(fig, axs, df, legend_args: Union[str, Mapping, Callable], *, set_size: Union[bool, Mapping] = True):
    if not callable(legend_args):
        legend_args_v = legend_args
        if not isinstance(legend_args_v, Mapping):
            legend_args_v = dict(label=legend_args_v)

        def legend_args(c):
            return legend_args_v
    if set_size is True:
        set_size = dict()

    ax = axs
    while isinstance(ax, Iterable):
        ax = ax[0]

    ax.set_prop_cycle(None)  # reset color cylce
    legends = []
    lowest_level_df = df
    while isinstance(lowest_level_df.columns, pd.MultiIndex):
        lowest_level_df = lowest_level_df[lowest_level_df.columns.levels[0][0]]
    valid_keys = {"marker", "c", "color", "ls", "linestyle", "label"}
    for c in sorted(lowest_level_df.columns):
        legends.append(ax.plot([], [], **{k: v for k, v in legend_args(c).items()
                                          if k in valid_keys})[0])
    fig.execute_constrained_layout()
    midpoint = (ax.get_position().x0 + axs[0, -1].get_position().x1) / 2
    anchor = (midpoint, 0.)

    if isinstance(set_size, Mapping):
        fig = set_latex_size(fig, ax, **set_size)
    else:
        fig.set_constrained_layout(False)

    ncol = len(legends)
    bbox_fig = np.array([[-1, -1], [2, 2]])
    leg = None
    while bbox_fig[0, 0] < 0 or bbox_fig[1, 0] > 1:
        if leg:
            leg.remove()
        leg = fig.legend(handles=legends, ncol=ncol, bbox_to_anchor=anchor, loc="upper center")

        bbox_display = leg.get_tightbbox(fig.canvas.get_renderer())
        bbox_fig = fig.transFigure.inverted().transform(bbox_display)
        ncol = ncol // 2

    if isinstance(set_size, Mapping):
        fig = set_latex_size(fig, ax, **set_size)

    return fig, axs


def set_legend_besides(fig, axs, df, legend_args: Union[str, Mapping, Callable], *, set_size: Union[bool, Mapping] = True):
    if not callable(legend_args):
        legend_args_v = legend_args
        if not isinstance(legend_args_v, Mapping):
            legend_args_v = dict(label=legend_args_v)

        def legend_args(c):
            return legend_args_v
    if set_size is True:
        set_size = dict()

    ax = axs
    while isinstance(ax, Iterable):
        ax = ax[0]

    legends = determine_legends(ax, df, legend_args)
    fig.execute_constrained_layout()
    midpoint = (axs[0, 0].get_position().y0 + axs[-1, 0].get_position().y1) / 2
    anchor = (1., midpoint)

    if isinstance(set_size, Mapping):
        fig = set_latex_size(fig, ax, **set_size)
    else:
        fig.set_constrained_layout(False)

    ncol = 1
    bbox_fig = np.array([[-1, -1], [2, 2]])
    leg = None
    while bbox_fig[0, 1] < 0 or bbox_fig[1, 1] > 1:
        if leg:
            leg.remove()
        leg = fig.legend(handles=legends, ncol=ncol, bbox_to_anchor=anchor, loc="center left")

        bbox_display = leg.get_tightbbox(fig.canvas.get_renderer())
        bbox_fig = fig.transFigure.inverted().transform(bbox_display)
        ncol = ncol + 1

    if isinstance(set_size, Mapping):
        fig = set_latex_size(fig, ax, **set_size)

    return fig, axs
