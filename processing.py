import os
import shutil
import subprocess
from collections import defaultdict, Iterable
from pathlib import Path
from typing import Union, List, Optional

import pandas as pd
import numpy as np
from datetime import datetime


idx = pd.IndexSlice


def to_list(o):
    return o if isinstance(o, list) else [o]


def quant(q):
    def _f(s):
        return np.quantile(s, q)
    _f.__name__ = "{:.0f}%".format(q * 100)
    return _f


def heuristic_values(df: Union[pd.DataFrame, pd.Series]):
    if isinstance(df, pd.Series):
        df.name = df.name or "value"
        return [df.name]
    else:
        if isinstance(df.index, pd.MultiIndex):
            return list(df.columns)
        else:
            values = []
            heuristic_values = "value runtime float_t".split()
            for hv in heuristic_values:
                if hv in df.reset_index().columns:
                    values.append(hv)
            if not values:
                raise RuntimeError("Could not determine values for grouping, please specify")
            else:
                return values


def apply_quantile(df: Union[pd.DataFrame, pd.Series], *, values=None, q: float = 0.9, **kwargs):
    values = to_list(values or heuristic_values(df))
    if isinstance(df.index, pd.MultiIndex):
        by = list(set(df.index.names) - {"rank"})
    else:
        by = list(set(df.columns) - set(values) - {"rank"})
    grouped = df.groupby(by, **kwargs)
    dfq = grouped.quantile(q)
    if isinstance(dfq, pd.DataFrame) and "rank" in dfq.columns:
        dfq = dfq.drop(columns="rank")
    return dfq


def pivot(df: Union[pd.DataFrame, pd.Series], columns, *, values=None):
    columns = to_list(columns)
    values = to_list(values or heuristic_values(df))
    if isinstance(df.index, pd.MultiIndex):
        df = df.reset_index()
    df = df.pivot_table(values=values, columns=columns, index=list(set(df.columns) - {*values, *columns}))
    return df


def keep_multiindex(df: Union[pd.DataFrame, pd.Series], keep: Union[str, List[str]]):
    keep = keep if not isinstance(keep, str) and isinstance(keep, Iterable) else [keep]
    if not set(keep) <= set(df.index.names):
        print("Warning: not all passed indices are within the dataframe's multiindex")
    return df.droplevel(level=list(set(df.index.names) - set(keep)), axis=0)


def reshape(df: Union[pd.DataFrame, pd.Series], values: Union[str, List[str]] = None, *,
            unstack: Union[str, List[str]], keep_indices: Union[str, List[str]]):
    unstack = [unstack] if isinstance(unstack, str) else unstack
    keep_indices = [keep_indices] if isinstance(keep_indices, str) else keep_indices
    if isinstance(df, pd.DataFrame) and values:
        df = df.loc[:, values]
    df = keep_multiindex(df, keep_indices + unstack)
    df = df.unstack(level=unstack)
    return df


def normalized_std(df: Union[pd.DataFrame, pd.Series], *, indices):
    df = df.reset_index() if isinstance(df.index, pd.MultiIndex) else df
    if "rank" not in df.columns:
        raise RuntimeError("Assumed 'rank' to be a column")
    g = df.groupby(list(set(df.columns) & set(indices) - {"rank"}))
    mean = g.mean().drop(columns="rank")
    std = g.std().drop(columns="rank")
    return (std / mean).describe(percentiles=[0.05, 0.25, 0.5, 0.75, 0.95])


def simplify(df: pd.DataFrame, values):
    values = [values] if isinstance(values, str) else values
    by = [col for col in df.columns if not col in ["rank"] + values]
    grouped = df.groupby(by)
    df_simplified = pd.DataFrame()
    for value in values:
        column = grouped[value].describe()
        column["std"] = column["std"] / column["mean"]
        column["what"] = value
        df_simplified = pd.concat([df_simplified, column])
    col_list = df_simplified.columns.to_list()
    df_simplified = df_simplified[["what"] + col_list[:-1]]
    df_simplified = df_simplified.drop(columns="count")
    df_simplified = df_simplified.reset_index()
    return df_simplified


def process_filename(filename: str, keys):
    if filename.endswith((".txt", ".csv")):
        filename = filename.split(".")[0]
    filename = filename.split('/')[-1]

    filename_components = filename.split("_")
    assert(len(filename_components) == len(keys))

    data = dict()

    for key, val in zip(keys, filename_components):
        data[key] = val

    return data


def process_raw(files, keys, regex):
    def _to_number(s):
        try:
            return int(s)
        except ValueError:
            return float(s)

    _regex = [r if isinstance(r, tuple) else (r, _to_number) for r in regex]

    data = list()
    for file in files:
        name_dict = process_filename(file, keys)
        file_dict = defaultdict(lambda: 0)
        with open(file, "r") as f:
            for l in f:
                for r, map in _regex:
                    match = r.match(l)
                    if match:
                        for name, val in match.groupdict().items():
                            file_dict[name] += map(val)
        data.append(dict(**name_dict, **file_dict))

    return pd.DataFrame(data)


def process_raw_regions(files, keys, regex, regions: Optional[List[str]] = None):
    def _to_number(s):
        try:
            return int(s)
        except ValueError:
            return float(s)

    default_region = "default"
    regions = regions or [default_region]

    _regex = [r if isinstance(r, tuple) else (r, _to_number) for r in regex]

    data = list()
    for file in files:
        name_dict = process_filename(file, keys)
        region_dicts = defaultdict(lambda: defaultdict(lambda: 0))
        active_region = "default"
        with open(file, "r") as f:
            for l in f:
                for r in regions:
                    active_region = r if r in l else active_region
                active_dict = region_dicts[active_region]
                for r, map in _regex:
                    match = r.match(l)
                    if match:
                        for name, val in match.groupdict().items():
                            active_dict[name] += map(val)
        if regions == [default_region]:
            data.append(dict(**name_dict, **region_dicts[default_region]))
        else:
            for region, region_dict in region_dicts.items():
                if region != default_region:
                    data.append(dict(**name_dict, **region_dicts["default"], **region_dict, region=region))

    return pd.DataFrame(data)

def to_file(df, basename, *, compress, timestamp):
    date = f"_{datetime.now().isoformat(timespec='seconds')}" if timestamp else ""
    filename = f"{basename}{date}.csv"
    df.to_csv(filename, index=False, sep=("," if compress else " "))
    if compress:
        subprocess.run(["gzip", "-f", filename])
        filename = f"{filename}.gz"
    thesis_path = Path.home() / "Documents" / "thesis" / "data"
    if thesis_path.exists():
        shutil.copy(filename, thesis_path)
