def input_filter(criterions):
    if criterions:
        def _filter(name):
            parts = name.split("_")
            for f in criterions:
                if f.startswith("!"):
                    success = all(p not in f[1:].split("|") for p in parts)
                elif f.startswith("@"):
                    eval_dict = dict()
                    for p in parts:
                        try:
                            k, v = p.split("-")
                            eval_dict[k] = int(v)
                        except ValueError:
                            pass

                    if eval_dict:
                        success = eval(f[1:], eval_dict)
                    else:
                        success = False
                else:
                    success = any(p in f.split("|") for p in parts)
                if not success:
                    return False
            return True
        return _filter
    else:
        return lambda x: True
